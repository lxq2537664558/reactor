#include <string>
#include <fcntl.h>
#include <netinet/tcp.h>
#include "logging.h"
#include "TcpConHandler.h"
#include "net.h"
using namespace std;
bool g_bExit = false;
TcpConHandler::TcpConHandler(reactor::handle_t handle) :
EventHandler(),
_handle(handle),
_state(eConned),
_reconnInterval(0)
{
	_inbuf.makeRoom();
	_outbuf.makeRoom();	
	_InitFd(handle);
	SUsedataPtr prt(new SUsedata);
	_user_data = prt;
}

TcpConHandler::TcpConHandler() :EventHandler(), _state(eDiscon), _reconnInterval(0)
{	
	_handle = -1;	
	_inbuf.makeRoom();
	_outbuf.makeRoom();
	SUsedataPtr prt(new SUsedata);
	_user_data = prt;
}

void TcpConHandler::_InitFd(reactor::handle_t handle)
{
	net::setNonBlock(handle);
	net::setReuseAddr(handle);
	net::setReusePort(handle);
	net::setNoDelay(handle);
	util::addFdFlag(handle, FD_CLOEXEC);
	int keepalive = 1; // 开启keepalive属性
	int keepidle = 30; // 如该连接在60秒内没有任何数据往来,则进行探测
	int keepinterval = 5; // 探测时发包的时间间隔为5 秒
	int keepcount = 3; // 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.
	setsockopt(handle, SOL_SOCKET, SO_KEEPALIVE, (void *)&keepalive, sizeof(keepalive));
	setsockopt(handle, SOL_TCP, TCP_KEEPIDLE, (void*)&keepidle, sizeof(keepidle));
	setsockopt(handle, SOL_TCP, TCP_KEEPINTVL, (void *)&keepinterval, sizeof(keepinterval));
	setsockopt(handle, SOL_TCP, TCP_KEEPCNT, (void *)&keepcount, sizeof(keepcount));
}

TcpConHandler::~TcpConHandler()
{	
	printf("~TcpConHandler()%p\n", this); 
}
bool TcpConHandler::Connect(const char* ip, unsigned short nport)
{
	if (_state != eDiscon)
	{
		warn("socket have connected\n");
		return true;
	}	
	_destip.clear();
	_destip.append(ip, strlen(ip));
	_port = nport;
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(nport);
	addr.sin_addr.s_addr = inet_addr(ip);

	_handle = socket(AF_INET, SOCK_STREAM, 0);
	fatalif(_handle < 0, "socket is not valid\n");	
	_InitFd(_handle);
	net::setNonBlock(_handle,false);
	int r = ::connect(_handle, (struct sockaddr *)&addr, sizeof(addr));
	net::setNonBlock(_handle, true);
	if (r != 0) {
		_state = eDiscon;
		close(_handle);
		return false;
	}	
	_state = eConned;
	return true;
}
void TcpConHandler::HandleWrite()
{		
	fatalif(eDiscon == _state, "socket discon,can not write\n");
	int len = _DySend();
	if (len > 0)
	{		
		printf("send response to client, fd=%d\n", (int)_handle);		
	}		
	g_reactor.RegisterHandler(shared_from_this(), reactor::kReadEvent);
}
ssize_t TcpConHandler::_DySend()
{
	size_t sended = 0;
	while (_outbuf.size() > sended) {
		ssize_t wd = ::write(_handle, _outbuf.data() + sended, _outbuf.size() - sended);
		if (wd > 0) {
			sended += wd;
			continue;
		}
		else if (wd == -1 && errno == EINTR) {
			continue;
		}
		else if (wd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
			g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);
			break;
		}
		else {
			break;
		}
	}
	if (sended>0)
		_outbuf.consume(sended);	
	return sended;
}

void TcpConHandler::HandleRead()
{		
	fatalif(eDiscon == _state, "socket discon,can not read\n");
	int r = _DyRev();
	if (r > 0)
	{			
		fatalif(_readCb == nullptr,"read call function is null\n");
		_readCb(shared_from_this());
		g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);
	}
	else
	{		
		if (-1==r && (errno == EAGAIN || errno == EWOULDBLOCK))
			g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);
		else
			HandleClose();
	}	
}

ssize_t TcpConHandler::_DyRev()
{
	int r = 0;
	while (_handle > 0) {
		_inbuf.makeRoom();
		int rd = 0;
		rd = ::read(_handle, _inbuf.end(), _inbuf.space());
		if (rd == -1 && errno == EINTR) { //fd中还有数据			
			continue;
		}
		else if (rd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {//fd中的数据读完			
			break;
		}
		else if (rd == 0 || rd == -1) {
			r = rd;
			break;
		}
		else { //rd > 0			
			r += rd;
			_inbuf.addSize(rd);
		}
	}
	return r;
}

void TcpConHandler::OnMsgCall(CodecBase* codec, const MsgCallBack& cb)
{
	_msgCallBack = cb;
	assert(!_readCb);	
	_codec.reset(codec);
	OnRead(
		[=](std::shared_ptr<EventHandler>handler)
	{		
		Slice msg;				
		int r = std::dynamic_pointer_cast<TcpConHandler>(handler)->_codec->tryDecode(_inbuf, msg);//从字节流中取出一条消息	
		string s;
		s.append(msg.toString().c_str(), msg.size());
		printf("rev %s\n", s.c_str());
		if (r>0)
		{
			_msgCallBack(shared_from_this(), msg.toString().c_str(), msg.size());
			_inbuf.consume(r);
		}		
		else{
			_inbuf.consume(_inbuf.size());
		}				
	}
	);
}
	


void TcpConHandler::HandleClose()
{
	fprintf(stderr, "client %d closed\n", _handle);
	std::shared_ptr<EventHandler> handler = shared_from_this();
	if (eConned == _state)
	{
		int r = g_reactor.RemoveHandler(handler);
		errorif(r != 0, "remove handler false\n");
		close(_handle);
		_state = eDiscon;
	}
	if (_reconnInterval>0)
	{
		heap_timer* reconntask = new heap_timer(_reconnInterval);			
		_user_data->host = _destip;
		_user_data->port = _port;
		_user_data->reconnectInterval = _reconnInterval;
		reconntask->cb_func = [=](SUsedataPtr user_data)
		{			
			TcpConHandlerPtr hler;
			hler=std::dynamic_pointer_cast<TcpConHandler>(handler);
			if (hler->Connect(user_data->host.c_str(), user_data->port))
			{				
				trace("reconnect to server %s %u success\n", user_data->host.c_str(), user_data->port);				
				g_reactor.RegisterHandler(handler, reactor::kReadEvent);
			}
			else
			{			
				g_reactor.RemoveHandler(handler);
				HandleClose();
			}
		};
		reconntask->user_data = _user_data;		
		trace("register a reconnect server task which will be run is %d second!\n", _reconnInterval);
		int r = g_reactor.RegisterTimerTask(reconntask);
		errorif(r != 0, "register time task false\n");		
	}
		
}



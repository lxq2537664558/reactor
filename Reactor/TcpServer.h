#pragma once
#include "TcpConHandler.h"
#include "net.h"
#define TcpServerPtr std::shared_ptr<TcpServer>
typedef std::function<std::shared_ptr<reactor::EventHandler>(reactor::handle_t handle)> OnClientCallBack;
class TcpServer : public reactor::EventHandler, private noncopyable
{
public:

	TcpServer() :EventHandler()
	{}

	~TcpServer(){ close(m_handle);	printf("~TcpServer() %p\n", this); }
	virtual bool Start(const char * ip, unsigned short port);
	virtual void HandleError()
	{
		fprintf(stderr, "client %d closed\n", m_handle);
		int r = g_reactor.RemoveHandler(shared_from_this());
		errorif(r != 0, "remove handler false\n");		
	}

	virtual reactor::handle_t GetHandle() const{return m_handle;}
	virtual void HandleWrite(){}
	virtual void HandleRead();
	virtual void OnClient(OnClientCallBack cb){ _onClientcb = cb; }

private:
	reactor::handle_t     m_handle;	
	OnClientCallBack _onClientcb;
};

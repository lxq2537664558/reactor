#pragma once
#include "TcpServer.h"
#include "SafeQueue.h"
#include "threads.h"
#define HSHAPtr std::shared_ptr<HSHA>
typedef std::function<void(EventHandlerPtr, const char* szByte, int len)> OnMsgCallBack;
typedef std::function<void()> Task;
class HSHA:public TcpServer
{
public:
	HSHA(int threads);
	~HSHA();	
	void OnMsg(CodecBase* codec, OnMsgCallBack cb);	
private:
	void _SafeCall(Task&& task);
	void _Wakeup();
private:
	OnMsgCallBack _msgcb;	
	CodecBase* _codec;
	ThreadPool threadPool_;
	SafeQueue<Task> tasks_;
	int wakeupFds_[2];
	TcpConHandlerPtr _handler;
	Buffer _buf;
};


#pragma once
#include "global.h"
#include "util.h"
#include "codec.h"
#include "Buffer.h"
#include "ikcp.h"
#include "net.h"
#define UdpConHandlerPtr std::shared_ptr<UdpHandler>
class UdpHandler :public reactor::EventHandler, private noncopyable
{
public:
	explicit UdpHandler();
	~UdpHandler();
	virtual void HandleRead();
	virtual void HandleWrite();	
	virtual bool Send(const char* szByte, int len, const char* ip, unsigned int port);
	virtual void Send(const char* szByte, int len);
	virtual void HandleClose(){g_reactor.RemoveHandler(shared_from_this());	close(_handle);	};
	virtual reactor::handle_t GetHandle() const{return _handle;}
	bool Connect(const char* ip, unsigned short nport);
	void OnMsgCall(CodecBase* codec, const MsgCallBackWithPeer& cb);
	void OnRead(const ReadCallBack& cb){ _readCb = cb; }
	bool Bind(const char* ip, short port, bool reusePort);
private:
	ssize_t _DyRev();
	ssize_t _DySend();
private:	
	reactor::handle_t _handle;
	ReadCallBack _readCb;
	std::unique_ptr<CodecBase> _codec;//解码函数回调
	Buffer _inbuf;
	Buffer _outbuf;	
	MsgCallBackWithPeer _msgCallBack;		//消息回调函数，将tcp连接，网络数据，回调给上层
	ikcpcb *_kcp = nullptr;	
	Ip4Addr _peer;
};


#pragma once
#include <thread>
#include <atomic>
#include <list>
#include <vector>
#include <functional>
#include <limits>
#include <condition_variable>
#include <mutex>
#include "SafeQueue.h"
#include "util.h"

typedef std::function<void()> Task;
struct ThreadPool: private noncopyable {
    //创建线程池
    ThreadPool(int threads, int taskCapacity=0, bool start=true);
    ~ThreadPool();
    void start();
    ThreadPool& exit() { tasks_.exit(); return *this; }
    void join();

    //队列满返回false
    bool addTask(Task&& task);
    bool addTask(Task& task) { return addTask(Task(task)); }
    size_t taskSize() { return tasks_.size(); }
private:
    SafeQueue<Task> tasks_;
    std::vector<std::thread> threads_;
};


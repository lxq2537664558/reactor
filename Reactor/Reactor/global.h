#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include "reactor.h"
#include "singleton.h"

class reactor::Reactor;

class Global : public Singleton<Global>
{
public:
    Global(void);
    ~Global(void);

    reactor::Reactor* g_reactor_ptr;
};

#define sGlobal Global::instance()
#define g_reactor (*(sGlobal->g_reactor_ptr))
#endif
typedef std::function<void(EventHandlerPtr, const char* szByte, int len)> MsgCallBack;
typedef std::function<void(EventHandlerPtr, const char* szByte, int len, const char* ip, short port)> MsgCallBackWithPeer;
typedef std::function<void(EventHandlerPtr)> ReadCallBack;

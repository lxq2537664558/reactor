#ifndef REACTOR_REACTOR_H_
#define REACTOR_REACTOR_H_

#include <stdint.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <memory>
#include "singleton.h"
#include "timeheap.h"
#define EventHandlerPtr std::shared_ptr<reactor::EventHandler>
namespace reactor
{
    typedef unsigned int event_t;
    enum
    {
        kReadEvent    = 0x01,		
        kWriteEvent   = 0x02,
        kErrorEvent   = 0x04,		
        kEventMask    = 0xff
    };

    typedef int handle_t;
	class EventHandler :public std::enable_shared_from_this<EventHandler>
    {
    public:

        virtual handle_t GetHandle() const = 0;

		virtual void HandleRead() {}

		virtual void HandleWrite(){}

		virtual void HandleClose(){}
		virtual void Send(const char* szByte, int len){}
		virtual bool Send(const char* szByte, int len,const char* ip,unsigned int port){}
    protected:

        EventHandler() {}

		virtual ~EventHandler() { printf("~EventHandler %p\n",this); }
    };

    class ReactorImplementation;

    class Reactor
    {
    public:

        Reactor();

        ~Reactor();

		int RegisterHandler(EventHandlerPtr handler, event_t evt);

		int RemoveHandler(EventHandlerPtr handler);

        void HandleEvents();

        int RegisterTimerTask(heap_timer* timerevent);

		void ReleaseHandlers();
    private:

        Reactor(const Reactor &);
        Reactor & operator=(const Reactor &);

    private:

        ReactorImplementation * m_reactor_impl;
    };

} // namespace reactor
#endif // REACTOR_REACTOR_H_


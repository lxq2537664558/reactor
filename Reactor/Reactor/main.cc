﻿#include <signal.h> 
#include <thread>
#include "TcpServer.h"
#include "HSHA.h"
#include "UdpHandler.h"
#include "KCPHandler.h"
#define _HSHA_

void printHelloworld(void* data)
{
	fprintf(stderr, "timertask : Hello world from timerTask!\n");	
}
void sigroutine(int dunno)
{ /* 信号处理例程，其中dunno将会得到信号的值 */

	switch (dunno) {
	case SIGINT:
		g_bExit = true;
		signal(SIGINT, SIG_DFL);
		break;
	}
	return;
}
void TEST_UDPCLEINT();
void TEST_UDPSERVER();
void TEST_TCPCLIENT();
void TEST_HSHA_TCPSERVER();
void TEST_TCPSERVER();
void TEST_KCP_CLIENT();
void TEST_KCP_SERVER();
int main(int argc, char ** argv)
{   
	//半同步半异步服务器用例测试
	//TEST_HSHA_TCPSERVER();
	//UDP客户端用例测试
	//TEST_UDPCLEINT();
	//TCP客户端测试
	//TEST_TCPCLIENT();
	//UDP服务器用例测试
	//TEST_UDPSERVER();
	//KCP客户端测试		
	//TEST_KCP_CLIENT();	
	//KCPSERVER测试	
	TEST_KCP_SERVER();
	
	return EXIT_SUCCESS;
}
void TEST_KCP_SERVER()
{
	Logger::getLogger().setFileName("log.txt");
	Logger::getLogger().setLogLevel(Logger::LTRACE);
	KCPHandlerPtr kcpHandlerPtr(new KCPHandler);
	kcpHandlerPtr->OnMsgCall(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len,const char*ip, short port)
	{		
		//handler->Send(szByte, len,ip, port);
		g_reactor.RegisterHandler(handler, reactor::kReadEvent);
	}
	);
	kcpHandlerPtr->StartUdpateTimer();
	std::string ip = "192.168.1.105";
	unsigned short port = 1200;
	if (!kcpHandlerPtr->Bind(ip.c_str(), port, true)){ error("Bind port failed, server started!\n"); kcpHandlerPtr->HandleClose(); }
	fprintf(stderr, "server started!\n");
	signal(SIGINT, sigroutine);
	int r;
	r = g_reactor.RegisterHandler(kcpHandlerPtr, reactor::kReadEvent);
	fatalif(r != 0, "register rcp server handler false\n");
	while (!g_bExit)
	{		
		g_reactor.HandleEvents();		
	}
	g_reactor.ReleaseHandlers();
	trace("Program exit\n");
}
void TEST_KCP_CLIENT()
{
	Logger::getLogger().setFileName("log.txt");
	Logger::getLogger().setLogLevel(Logger::LTRACE);
	KCPHandlerPtr kcpHandlerPtr(new KCPHandler);
	kcpHandlerPtr->OnMsgCall(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len,const char* ip,short port)
	{
		handler->Send(szByte, len,ip,port);
	}
	);
	kcpHandlerPtr->StartUdpateTimer();
	std::string ip = "192.168.1.105";
	unsigned short port = 1200;
	std::string msg = "hello\n";
	if (!kcpHandlerPtr->Send(msg.c_str(), msg.length(), ip.c_str(), port))
	{ 
		kcpHandlerPtr->HandleClose(); 
		trace("send %s %d failed\n",ip, port);
	}		
	g_reactor.RegisterHandler(kcpHandlerPtr, reactor::kWriteEvent);	
	signal(SIGINT, sigroutine);
	fprintf(stderr, "client started!\n");
	int r;
	while (!g_bExit)
	{		
		g_reactor.HandleEvents();		
	}
	g_reactor.ReleaseHandlers();
	printf("Program exit\n");
}

void TEST_UDPSERVER()
{
	UdpConHandlerPtr udpHandlerPtr(new UdpHandler);	
	udpHandlerPtr->OnMsgCall(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len,const char* ip,short port)
	{
		handler->Send(szByte, len,ip,port);		
	}
	);
	std::string ip = "192.168.1.105";
	unsigned short port = 1200;
	if (!udpHandlerPtr->Bind(ip.c_str(), port, true)){ error("Bind port failed, server started!\n"); udpHandlerPtr->HandleClose(); }
	printf("server start\n");
	signal(SIGINT, sigroutine);		
	int r;
	r = g_reactor.RegisterHandler(udpHandlerPtr, reactor::kReadEvent);//服务器端需要不断read
	fatalif(r != 0, "register udp server handler false\n");	
	
	while (!g_bExit)
	{												
		g_reactor.HandleEvents();								
	}
	g_reactor.ReleaseHandlers();
	trace("Program exit\n");
}

void TEST_UDPCLEINT()
{
	Logger::getLogger().setFileName("log.txt");
	Logger::getLogger().setLogLevel(Logger::LTRACE);
	UdpConHandlerPtr udpHandlerPtr(new UdpHandler);
	udpHandlerPtr->OnMsgCall(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len,const char* ip,short port)
	{
		handler->Send(szByte, len,ip,port);
	}
	);
	std::string ip = "192.168.1.108";
	unsigned short port = 1206;	
	std::string msg = "hello\n";
	if (udpHandlerPtr->Send(msg.c_str(), msg.length(), ip.c_str(), port)){ error("send data failed\n"); }	
	signal(SIGINT, sigroutine);		
	int r;

	while (!g_bExit)
	{				
		g_reactor.HandleEvents();		
	}
	g_reactor.ReleaseHandlers();
	printf("Program exit\n");

}
void TEST_TCPCLIENT()
{
	TcpConHandlerPtr tcpConHandler(new TcpConHandler);
	tcpConHandler->OnMsgCall(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len)
	{
		handler->Send(szByte, len);
	}
	);
	std::string ip = "192.168.1.108";
	unsigned short port = 1201;
	tcpConHandler->SetReconnInterval(20);
	if (!tcpConHandler->Connect(ip.c_str(), port))
	{
		tcpConHandler->HandleClose();
	}
	else
	{
		std::string msg = "hello\n";
		tcpConHandler->Send(msg.c_str(), msg.length());
		trace("connect to server %s %u success\n", ip.c_str(), port);
	}

	signal(SIGINT, sigroutine);
	fprintf(stderr, "server started!\n");
	int r;
	while (!g_bExit)
	{		
		g_reactor.HandleEvents();
	}
	g_reactor.ReleaseHandlers();
	printf("Program exit\n");
}
void TEST_HSHA_TCPSERVER()
{
	Logger::getLogger().setLogLevel(Logger::LTRACE);
	HSHAPtr hsha(new HSHA(10));
	exitif(hsha->Start("192.168.1.105", 1200) == false, "Server Start false, please debug hsha.Start\n");
	hsha->OnMsg(new MsgCodec, [&](EventHandlerPtr handler, const char* szByte, int len)
	{
		std::string xml = std::string("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
			+ "<PACKET TYPE=\"RESPONSE\" >"
			+ "<HEAD><TRAN_CODE>11</TRAN_CODE><RCPT_NO>0000000000000000001</RCPT_NO> <RSLT_CODE>000000</RSLT_CODE><RSLT_MSG>成功</RSLT_MSG></HEAD>"
			+ "<BODY><BASE><PAY_APP_NO>000000010001</PAY_APP_NO><AMOUNT>000000001000</AMOUNT><REMARK>小明</REMARK><CHECK_CODE>00000001</CHECK_CODE></BASE></BODY>"
			+ "</PACKET>";
		std::string utf8xml = util::GBToUTF8(xml);
		handler->Send(utf8xml.c_str(), utf8xml.length());
		printf("send xml \n%s\n", utf8xml.c_str());
	}
	);

	signal(SIGINT, sigroutine);
	fprintf(stderr, "server started!\n");
	int r;
	while (!g_bExit)
	{
		r = g_reactor.RegisterHandler(hsha, reactor::kReadEvent);//服务器端需要不断read
		fatalif(r != 0, "register rcp server handler false\n");
		g_reactor.HandleEvents();
	}
	g_reactor.ReleaseHandlers();
	printf("Program exit\n");

}
void TEST_TCPSERVER()
{
	Logger::getLogger().setLogLevel(Logger::LTRACE);
	TcpServerPtr  server(new TcpServer);
	if (!server->Start("192.168.1.105", 1200))
	{
		fprintf(stderr, "start server failed\n");
		return ;
	}
	server->OnClient([=](reactor::handle_t handle){
		TcpConHandlerPtr handler(new TcpConHandler(handle));
		handler->OnMsgCall(new MsgCodec, [&](EventHandlerPtr hler, const char* szByte, int len) {
			sleep(3);
			hler->Send(szByte, len);
		});
		return handler;
	}
	);
	signal(SIGINT, sigroutine);
	fprintf(stderr, "server started!\n");
	int r;
	while (!g_bExit)
	{
		r = g_reactor.RegisterHandler(server, reactor::kReadEvent);
		fatalif(r != 0, "register rcp server handler false\n");
		g_reactor.HandleEvents();
	}
	g_reactor.ReleaseHandlers();
	printf("Program exit\n");
}


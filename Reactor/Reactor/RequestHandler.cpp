#include <string>
#include <netinet/tcp.h>
#include "logging.h"
#include "TcpConHandler.h"
#include "net.h"
using namespace std;

TcpConHandler::TcpConHandler(reactor::handle_t handle) :
EventHandler(),
_handle(handle)
{
	_inbuf.makeRoom();
	_outbuf.makeRoom();
	Net::SetNonBlock(handle);
	int keepalive = 1; // 开启keepalive属性
	int keepidle = 30; // 如该连接在60秒内没有任何数据往来,则进行探测
	int keepinterval = 5; // 探测时发包的时间间隔为5 秒
	int keepcount = 3; // 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.
	setsockopt(handle, SOL_SOCKET, SO_KEEPALIVE, (void *)&keepalive, sizeof(keepalive));
	setsockopt(handle, SOL_TCP, TCP_KEEPIDLE, (void*)&keepidle, sizeof(keepidle));
	setsockopt(handle, SOL_TCP, TCP_KEEPINTVL, (void *)&keepinterval, sizeof(keepinterval));
	setsockopt(handle, SOL_TCP, TCP_KEEPCNT, (void *)&keepcount, sizeof(keepcount));
}
void TcpConHandler::HandleWrite()
{		
	int len = _DySend();
	if (len > 0)
	{		
		printf("send response to client, fd=%d\n", (int)_handle);		
	}		
}
ssize_t TcpConHandler::_DySend()
{
	size_t sended = 0;
	while (_outbuf.size() > sended) {
		ssize_t wd = ::write(_handle, _outbuf.data() + sended, _outbuf.size() - sended);
		if (wd > 0) {
			sended += wd;
			continue;
		}
		else if (wd == -1 && errno == EINTR) {
			continue;
		}
		else if (wd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
			g_reactor.RegisterHandler(this, reactor::kWriteEvent);
			break;
		}
		else {
			break;
		}
	}
	if (sended>0)
		_outbuf.consume(sended);	
	return sended;
}

void TcpConHandler::HandleRead()
{		
	int r = _DyRev();
	if (r > 0)
	{				
		_readCb();					
	}
	else
	{
		if (!((errno == EAGAIN || errno == EWOULDBLOCK) && (r == -1)))
			Close();		
	}
}

ssize_t TcpConHandler::_DyRev()
{
	int r = 0;
	while (_handle > 0) {
		_inbuf.makeRoom();
		int rd = 0;
		rd = ::read(_handle, _inbuf.end(), _inbuf.space());
		if (rd == -1 && errno == EINTR) {
			if (rd > 0)
			{
				r += rd;	
				_inbuf.addSize(rd);
			}				
			continue;
		}
		else if (rd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
			if (rd > 0)
				r += rd;
			break;
		}
		else if (rd == 0 || rd == -1) {
			r = rd;
			break;
		}
		else { //rd > 0			
			r += rd;
			_inbuf.addSize(rd);
		}
	}
	return r;
}

void TcpConHandler::OnMsgCall(CodecBase* codec, const MsgCallBack& cb)
{
	_msgCallBack = cb;
	assert(!_readCb);	
	if (_codec)
	{
		delete _codec;
		_codec = nullptr;
	}
	_codec = codec;
	OnRead(
	[=]()
	{		
		Slice msg;
		printf("rev buf %s\n", _inbuf.data());
		int r = _codec->tryDecode(_inbuf,msg);//从字节流中取出一条消息					
		//todo 
		/*
		1.	需要返回解码失败的错误码，以及错误码描述		
		*/
		if (r>0)
		{
			_msgCallBack(this, msg.toString().c_str(), msg.size());
			_inbuf.consume(r);
		}		
		else{
			_inbuf.consume(_inbuf.size());
		}		
	}
	);
}


#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <memory>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <string>
#include <functional>
#include "logging.h"
#include "global.h"
#include "codec.h"
#include "Buffer.h"
#include "SafeQueue.h"
extern bool g_bExit;
#define TcpConHandlerPtr std::shared_ptr<TcpConHandler>
class TcpConHandler : public reactor::EventHandler, private noncopyable
{
public:
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: TcpConHandler构造函数， 当监听fd获得client时，使用该构造函数
	输入参数: handle:文件描述符
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	explicit TcpConHandler(reactor::handle_t handle);
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 主动连接服务端时，使用该构造函数
	输入参数: 
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	explicit TcpConHandler();
	~TcpConHandler();	
	virtual reactor::handle_t GetHandle() const
	{
		return _handle;
	}
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 处理数据写的操作，向与对端建立连接的fd写入数据。epoll通知可写操作时触发该函数。
	输入参数: 
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	virtual void HandleWrite();
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 处理数据读的操作，向与对端建立连接的fd读取数据。epoll通知可读操作时触发该函数。
	输入参数: 
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	virtual void HandleRead();	
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 将需要发送的数据添加到发送缓冲区，并修改事件状态。当epoll触发事件时调用HandleWrite函数
	输入参数: 
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	virtual void Send(const char* szByte, int len){ _outbuf.append(szByte, len); g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent); }
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 处理连接的关闭操作。如果连接设置了重连时间，会触发连接重连动作
	输入参数: 
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	virtual void HandleClose();	
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 连接服务端
	输入参数: ip:服务器ip	nport:服务端端口
	输出参数: 无
	返 回 值: true:连接服务端成功		false:连接服务端失败
	备    注: 
	*************************************************************************************/
	bool Connect(const char* ip, unsigned short nport);	
	void OnMsgCall(CodecBase* codec, const MsgCallBack& cb);
	void OnRead(const ReadCallBack& cb){ _readCb = cb; }		
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 设置重连时间间隔
	输入参数: second:重连的秒数
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	void SetReconnInterval(int second) { _reconnInterval = second; }			
private:	
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 往与建立连接的fd写入数据
	输入参数: 
	输出参数: 无
	返 回 值: 返回写入的字节数
	备    注: 
	*************************************************************************************/
	ssize_t _DySend();

	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 从与建立连接的fd读取数据
	输入参数: 
	输出参数: 无
	返 回 值: 返回读取的字节数
	备    注: 
	*************************************************************************************/
	ssize_t _DyRev();
	/*************************************************************************************
	作    者: ouyongjiu
	函数功能: 初始化fd
	输入参数: handle:文件描述符
	输出参数: 无
	返 回 值: 无
	备    注: 
	*************************************************************************************/
	void _InitFd(reactor::handle_t handle);
private:	
	enum EState
	{		
		eConned,
		eDiscon
	};
	EState _state;					//fd的状态
	reactor::handle_t _handle;		//fd
	Buffer _inbuf;					//输入缓冲区，存储从fd中读取的数据
	Buffer _outbuf;					//输出缓冲区，存储向fd写入的数据
	MsgCallBack _msgCallBack;		//消息回调函数，将tcp连接，网络数据，回调给上层
	ReadCallBack _readCb;			//读回调函数，此处判断读取的数据是否可以解码
	std::unique_ptr<CodecBase> _codec;//解码函数回调
	std::string _destip;			//对端ip
	unsigned short _port;			//对端端口
	int _reconnInterval;			//reconnect time interval
	SUsedataPtr _user_data;			//重连任务回传的数据
};

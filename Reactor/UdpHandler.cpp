#include <fcntl.h>
#include <string>
#include <assert.h>
#include "UdpHandler.h"
#include "net.h"
#include "logging.h"
#include <arpa/inet.h>
#include <sys/time.h>
using namespace std;

UdpHandler::UdpHandler():EventHandler()
{
	_handle = socket(AF_INET, SOCK_DGRAM, 0);
	fatalif(_handle < 0, "socket failed %d %s", errno, strerror(errno));
	net::setReuseAddr(_handle);
	net::setReusePort(_handle);
	net::setNoDelay(_handle);	
	net::setNonBlock(_handle);
	int t = util::addFdFlag(_handle, FD_CLOEXEC);
	fatalif(t, "addFdFlag FD_CLOEXEC failed %d %s", t, strerror(t));	
}

UdpHandler::~UdpHandler()
{	
	close(_handle);
}
void UdpHandler::OnMsgCall(CodecBase* codec, const MsgCallBackWithPeer& cb)
{
	_msgCallBack = cb;
	assert(!_readCb);
	_codec.reset(codec);
	OnRead(
		[=](std::shared_ptr<EventHandler>handler)
	{
		Slice msg;
		int r = std::dynamic_pointer_cast<UdpHandler>(handler)->_codec->tryDecode(_inbuf, msg);//从字节流中取出一条消息			
		if (r > 0)
		{
			_msgCallBack(shared_from_this(), msg.toString().c_str(), msg.size(),_peer.ip().c_str(),_peer.port());
			_inbuf.consume(r);
		}
		else{
			_inbuf.consume(_inbuf.size());
		}
	});
}
void UdpHandler::HandleRead()
{
	int r = _DyRev();
	if (r > 0)
	{
		fatalif(_readCb == nullptr, "read call function is null\n");
		_readCb(shared_from_this());		
	}
	else
	{
		if (-1 == r && (errno == EAGAIN || errno == EWOULDBLOCK))
		{
			g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);				
		}			
	}
}


ssize_t UdpHandler::_DySend()
{	
	size_t sended = 0;	
	socklen_t len = sizeof(struct sockaddr_in);
	while (_outbuf.size() > sended) {
		ssize_t wd = ::sendto(_handle, _outbuf.data() + sended, _outbuf.size() - sended, 0, (struct sockaddr *)&_peer.getAddr(),len);
		if (wd > 0) {
			sended += wd;
			continue;
		}
		else if (wd == -1 && errno == EINTR) {
			continue;
		}
		else if (wd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {//写完数据		
			break;
		}
		else {
			error("errno %d \n", errno);
			break;
		}		
	}
	if (sended > 0)
		_outbuf.consume(sended);	
	return sended;
}
ssize_t UdpHandler::_DyRev()
{
	int r = 0;	
	struct sockaddr_in cli_addr;	
	socklen_t len = sizeof(cli_addr);
	while (_handle > 0) {
		_inbuf.makeRoom();
		int rd = 0;
		rd = recvfrom(_handle, _inbuf.end(), _inbuf.space(), 0, (struct sockaddr *)&cli_addr, &len);				
		if (rd == -1 && errno == EINTR) { //fd中还有数据			
			continue;
		}
		else if (rd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {//fd中的数据读完			
			break;
		}
		else if (rd == 0 || rd == -1) {
			r = rd;
			break;
		}
		else { //rd > 0			
			r += rd;
			_inbuf.addSize(rd);
		}
	}		
	_peer = Ip4Addr(cli_addr);
	return r;
}



void UdpHandler::HandleWrite()
{	
	int len = _DySend();
	if (len > 0)
	{
		trace("send response to client, fd=%d\n", (int)_handle);
	}		
	g_reactor.RegisterHandler(shared_from_this(), reactor::kReadEvent);	
}
void UdpHandler::Send(const char* szByte, int len)
{	
	_outbuf.makeRoom(); _outbuf.append(szByte, len);	
	g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);
}
bool UdpHandler::Send(const char* szByte, int len, const char* ip, unsigned int port){
	if (!Connect(ip, port)){		
		return false;
	}
	_outbuf.makeRoom(); _outbuf.append(szByte, len);
	_peer = Ip4Addr(ip, port);
	g_reactor.RegisterHandler(shared_from_this(), reactor::kWriteEvent);	
	return true;
}
bool UdpHandler::Connect(const char* ip, unsigned short nport)
{		
	Ip4Addr addr(string(ip), nport);	
	/*int r = ::connect(_handle, (sockaddr *)&addr.getAddr(), sizeof(sockaddr_in));
	if (r != 0 && errno != EINPROGRESS) {
	error("connect to %s error %d %s", addr.toString().c_str(), errno, strerror(errno));
	return false;
	}	*/
	_peer = addr;	//保存对端地址
	return true;
}

bool UdpHandler::Bind(const char* ip, short port, bool reusePort) {	
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);	
	if (::bind(_handle, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		error("Bind %s %d failed\n",ip,port);
		return false;
	}	
	return true;
}
#include "protocol.h"
using namespace std;
#include <string.h>
#include "json/json.h"
Protocol::Protocol()
{

}
Protocol::~Protocol()
{

}
std::string Protocol::packageLoginMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8],unsigned char pwd[32])
{
	string strmsg;	
	MsgHeader baseHeader = _packageHeader(MSG_LOGIN_MSG, sender, rever, sessionID,32);
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));
	strmsg.append((char*)pwd, 32);
	return strmsg;
}

std::string Protocol::packageLoginMsgRsp(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8],unsigned char returnCode[2])
{
	string strmsg;
	MsgHeader baseHeader = _packageHeader(MSG_LOGIN_MSG_RSP, sender, rever, sessionID, 2);
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));
	strmsg.append((char*)returnCode, 2);
	return strmsg;
}

std::string Protocol::packageChatMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8], std::string& strchat)
{
	string strmsg;	
	MsgHeader baseHeader = _packageHeader(MSG_CHAT_MSG, sender, rever, sessionID, strchat.length());
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));
	strmsg.append(strchat.c_str(), strchat.length());
	return strmsg;
}

std::string Protocol::packageChatMsgRsp(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8])
{
	string strmsg;
	MsgHeader baseHeader = _packageHeader(MSG_CHAT_MSG_RSP, sender, rever, sessionID, 0);
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));	
	return strmsg;
}

std::string Protocol::packageMassMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8], std::string& strchat)
{
	string strmsg;
	MsgHeader baseHeader = _packageHeader(MSG_MASS_MSG, sender, rever, sessionID, strchat.length());
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));
	return strmsg;
}

std::string Protocol::packageHeatbreakMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8])
{
	string strmsg;	
	MsgHeader baseHeader = _packageHeader(MSG_HEARBREAK_MSG, sender, rever, sessionID, 0);
	strmsg.append((char*)&baseHeader, sizeof(baseHeader));
	return strmsg;
}

MsgHeader Protocol::_packageHeader(unsigned short cmd, unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8],int contxLen)
{
	MsgHeader baseHeader;	
	baseHeader.cmd = cmd;	
	memcpy(baseHeader.sender, sender, 32);
	memcpy(baseHeader.receiver, rever, 32);
	memcpy(baseHeader.sessionID, sessionID, 8);
	baseHeader.contxLen = contxLen;
	baseHeader.Hton();
	return baseHeader;
}

std::string Protocol::packageTradeMsgRsp()
{
	Json::Value content;
	content["RespCode"] = "0000";
	content["Result"] = "Parse json error";

	Json::Value msg;
	msg["Content"] = content;
	Json::FastWriter fast_writer;
	std::string strJson = fast_writer.write(msg).c_str();
	printf("%s\n", strJson.c_str());
	return strJson;
}
#pragma once
#include <string>
#include <handy/handy.h>
class MsgHandler
{
public:
	MsgHandler();
	~MsgHandler();	
	std::string HandleMsg(const handy::TcpConnPtr& con, const char* szData, int len);
private:
	std::string _HandleLoginMsg( const char* szData, int len, bool& bLogin);
	void _HandleLoginMsgRsp(const char* szData, int len);
	std::string _HandleHeartbreakMsg(const char* szData, int len);
	void _HandleChatMsg(const handy::TcpConnPtr& con,const char* szData, int len);
	void _HandleMassMsg(const char* szData, int len);
};


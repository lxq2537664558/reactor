#include <handy/handy.h>
#include "protocol.h"
#include "UserManager.h"
#include "Utility.h"
#include "MsgHandler.h"
#include "serveripconfig.h"
#include "MsgCodec.h"
using namespace std;
using namespace handy;

void TEST_CHAT(EventBase& base);
void TEST_MASS_MSG(EventBase& base);
int main(int argc, const char* argv[]) {
	Logger::getLogger().setFileName("handy.log");
	Logger::getLogger().setRotateInterval(180);			
	setloglevel("INFO");	
	EventBase base;
#ifndef CHAT_CLIENT
	HSHAPtr hsha = HSHA::startServer(&base, severip.c_str(), 1200, 4);
	exitif(!hsha, "bind failed");	
	Signal::signal(SIGINT, [&, hsha]{ base.exit(); hsha->exit(); signal(SIGINT, SIG_DFL); });
	hsha->onMsg(new MsgCodec, [&](const TcpConnPtr& con, const string& input){
		MsgHandler msgHandler;
		string output=msgHandler.HandleMsg(con,input.c_str(),input.length());		
		return output;
	});
	hsha->server_->onConnState([](const TcpConnPtr& con) {
		if (con->getState() == TcpConn::Closed) {				
			if (gUserManager->RemoveUser(con))
				info("close fd %d and release con\n", con->getChannel()->fd());
			else
				info("can not find user for remove\n");
		}
	});

#else
	TEST_CHAT(base);
	TEST_MASS_MSG(base);
#endif

	base.loop();
	info("program exited");
}

void TEST_MASS_MSG(EventBase& base)
{
	base.runAfter(1000, [&]
	{
		TcpConnPtr con = TcpConn::createConnection(&base, severip.c_str(), 1200);
		con->onMsg(new MsgCodec, [](const TcpConnPtr& con, Slice msg) {
			string input = msg;
			MsgHandler msgHandler;
			msgHandler.HandleMsg(con, input.c_str(), input.length());

		});
		con->onState([](const TcpConnPtr& con) {
			if (con->getState() == TcpConn::Connected) {
				Protocol  protocol;
				string sender = "ouyj";
				string revder = "sysyem@com.cn";
				string session = "00000000";
				string pwd = "123456";
				string loginmsg = protocol.packageLoginMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), (unsigned char*)pwd.c_str());
				con->sendMsg(loginmsg);
			}
		});
		base.runAfter(3000, [=]{
			Protocol  protocol;
			string sender = "ouyj";
			string revder = "qun1";
			string session = "00000000";
			string chatcontx = "hello, all the member";
			chatcontx = CUtility::GBToUTF8(chatcontx);
			string chatmsg;
			chatmsg = protocol.packageMassMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), chatcontx);
			con->sendMsg(chatmsg);
		});
		string client[3];
		client[0] = "m1@com.cn";
		client[1] = "m2@com.cn";
		client[2] = "m3@com.cn";
		for (int i = 0; i < 3; i++)
		{
			TcpConnPtr con = TcpConn::createConnection(&base, severip.c_str(), 1200);
			con->onMsg(new MsgCodec, [](const TcpConnPtr& con, Slice msg) {
				string input = msg;
				MsgHandler msgHandler;
				msgHandler.HandleMsg(con, input.c_str(), input.length());

			});
			con->onState([=](const TcpConnPtr& con) {
				if (con->getState() == TcpConn::Connected) {
					Protocol  protocol;
					string sender = client[i];
					string revder = "sysyem@com.cn";
					string session = "00000000";
					string pwd = "123456";
					string loginmsg = protocol.packageLoginMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), (unsigned char*)pwd.c_str());
					con->sendMsg(loginmsg);
				}
			});
		}


	}
	);
}

void TEST_CHAT(EventBase& base)
{
	base.runAfter(1000, [&]
	{
		TcpConnPtr con = TcpConn::createConnection(&base, severip.c_str(), 1200);
		con->onMsg(new MsgCodec, [](const TcpConnPtr& con, Slice msg) {
			string input = msg;
			MsgHandler msgHandler;
			msgHandler.HandleMsg(con, input.c_str(), input.length());

		});
		con->onState([](const TcpConnPtr& con) {
			if (con->getState() == TcpConn::Connected) {
				Protocol  protocol;
				string sender = "Lilin@com.cn";
				string revder = "sysyem@com.cn";
				string session = "00000000";
				string pwd = "123456";
				string chatcontx = "hello，我的世界";
				chatcontx = CUtility::GBToUTF8(chatcontx);
				string loginmsg = protocol.packageLoginMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), (unsigned char*)pwd.c_str());
				con->sendMsg(loginmsg);
			}
		});
		base.runAfter(3000, [=]{
			Protocol  protocol;
			string sender = "Lilin@com.cn";
			string revder = "Lili@com.cn";
			string pwd = "123456";
			string session = "00000000";
			string chatcontx = "hello，我的世界";
			chatcontx = CUtility::GBToUTF8(chatcontx);
			string chatmsg;
			chatmsg = protocol.packageChatMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), chatcontx);
			con->sendMsg(chatmsg);
		});

		base.runAfter(8000, [=]{
			Protocol  protocol;
			string sender = "Lilin@com.cn";
			string revder = "Lili@com.cn";
			string session = "00000000";
			string chatcontx = "hello,Lili@com.cn,i am Lilin@com.cn";
			chatcontx = CUtility::GBToUTF8(chatcontx);
			string chatmsg;
			chatmsg = protocol.packageChatMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), chatcontx);
			con->sendMsg(chatmsg);
		});
	}
	);

	base.runAfter(6000, [&]
	{
		TcpConnPtr con = TcpConn::createConnection(&base, severip.c_str(), 1200);
		con->onMsg(new MsgCodec, [](const TcpConnPtr& con, Slice msg) {
			string input = msg;
			MsgHandler msgHandler;
			msgHandler.HandleMsg(con, input.c_str(), input.length());
		});
		con->onState([](const TcpConnPtr& con) {
			if (con->getState() == TcpConn::Connected) {
				Protocol  protocol;
				string sender = "Lili@com.cn";
				string revder = "Lilin@com.cn";
				string pwd = "123456";
				string session = "00000000";
				string loginmsg = protocol.packageLoginMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), (unsigned char*)pwd.c_str());
				con->sendMsg(loginmsg);

				string chatcontx = "hello,Lilin@com.cn";
				chatcontx = CUtility::GBToUTF8(chatcontx);
				string chatmsg;
				chatmsg = protocol.packageChatMsg((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), chatcontx);
				con->sendMsg(chatmsg);
			}
		});
	}
	);
}

#pragma once
#include <map>
#include <string>
#include <handy/handy.h>
using namespace handy;
class UserManager
{
public:	
	static UserManager* GetInstance(){ static UserManager  usermanager; return &usermanager; }
	~UserManager();
	bool AddUser(std::string amonut, const TcpConnPtr& tcpPtr);
	bool RemoveUser(const TcpConnPtr& tcpPtr);
	bool FindUser(std::string amonut, TcpConnPtr& tcpPtr);
private:
	UserManager(){}
private:
	std::map<std::string, TcpConnPtr>_userConnptr;
	std::map<int, std::string>_fd2user;
	std::mutex _mtx;           // mutex for critical section	
};
#define gUserManager UserManager::GetInstance()


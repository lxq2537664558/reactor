#pragma once
#include <handy/handy.h>
class MsgCodec : public handy::CodecBase
{
public:
	MsgCodec(){}
	virtual ~MsgCodec(){}
	int tryDecode(handy::Slice data, handy::Slice& msg) override;
	void encode(handy::Slice msg, handy::Buffer& buf) override;
	handy::CodecBase* clone() override { return new MsgCodec(); }
};




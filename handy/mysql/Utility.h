#pragma once
#include <string> 
#include <vector>
#include <map>
using namespace std; 

#define    LICENCE_CHECKING_FLAG    0
#define    ORIGINAL_TIME_XOR_BIT    0x0f0f0f0f0f0f0f0f
#define    CURRENT_TIME_XOR_BIT     0xf0f0f0f0f0f0f0f0

#define    KEYBOARD_INPUT_ENABLE    0

#if    KEYBOARD_INPUT_ENABLE
#define    CUtility_Printf    CUtility::PrintfMsg
#else
void CUtility_SaveLogFromMemoryToDisk();
void CUtility_Printf(const char* format, ...);
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) if(x){delete x;x=NULL;}
#endif

#ifndef INT64
#define INT64  long long
#endif

struct  FilePathInfo{
	string m_strName;
	string m_strFullPath;
	bool   m_bFind;	//从设备表中找到了路径.
};

struct FolderInfo
{
	FilePathInfo m_path;
	vector<FilePathInfo >m_filesPath;
};

extern void msleep( unsigned int ms );
class CUtility  
{
public:
	CUtility();
	virtual ~CUtility();

    static string GetExeFilePath();


	//****文件相关*****//
    //取得自1970年1月1日0时以来的毫秒数
    static INT64  GetTickTime();

    static unsigned int GetTickCountOld();
	//得到文件大小
	static int GetFileSize(const char *szFileName);
	
	static bool IsFileExist(const char *szFilePath);
    static bool IsDirExist(const char* szDirPath);
    static string UintToString(unsigned int nNum);
	//LOG
    static bool WriteStrToFile( const char * pszContent,int nLength,const char *szName="c:\\log.txt",bool bAppend=true );
    static bool WriteStrToFile(FILE * pFile, const char * pszContent,int nLength);
	//默认长度=0时读全文件
	static int ReadStrFromFile( char * pszContent,const char *szName,int nLength=0);
	//****文件相关*****//
    static string ReadStrFromFile(const char *szName);
    static string GetCurHostIP();
    //磁盘相关
    //返回可用的空间，单位MB
    static int GetDiskFreeSpace(const char *szPath=NULL);
    //返回总空间
    static int GetDiskTotalSpace(const char*szPath=NULL);
	//日期相关
	//获取DATA,"2011-5-2"
	static string  GetCurDate();
    //获取当前时间,hourminsec "231112"
    static string GetCurTimeInOneDay();
	//获取详细时间 "2011-09-05 11:25:12"
    static string GetFullTime();
	//获取详细时间 "20110905112512"	
	static string GetFullTimeNoInterVal();
    //获取当前秒和毫秒
    static void GetTimeAndTick(unsigned int &nSec,unsigned int &nMsec);
	//获取当前时间的秒数
    static long GetCurrentTimeSeconds();
	//获取当前时间秒数的字符串
    static string GetCurrentTimeSecondsString();
	//获取当前时间 "月日时分年.秒"
    static string GetCurrentFullTimeInLinux();

    //for gbtime
    static string GetTimeByTimeT(time_t timeP);

    static bool IfNeededChangeTime(const string& strTime,const string& strTimeString);
	//目录相关
	//isFile 路径是否为文件.
    static int CreateFolder(const char* strPath,bool isFile=false);
	
	static void ListDirInPath(const char *path, vector<FilePathInfo> &mapDir);
	
	static void ListFileInPath(const char *path, map<string ,FilePathInfo> &mapDir,const char *szExt);
	
	static void DeleteFile(const char *path,bool bDelEmptyFolder);
    
    static void DeleteFileRecursive(const char *path);

	//16进制内存值转16进制字符串
	//@param :[in]  szInBuf 输入的内存数组.
	//@param :[in]  nInBufLength  内存数组长度.
	//@param :[out] szOutBuf 输出的字符串数组内容.必须在外部申请,长度为输入长度的两倍.
	//@param :[IN]  bLower 是否输出小写字母;
	static bool HexToAscii(const unsigned char *szInBuf,int nInBufLength,unsigned char *szOutBuf,bool bLower=false);
	//16进制字符串转16进制内存值
	//@param :[in]  szInBuf 输入的字符串数组.
	//@param :[in]  nInBufLength  字符串数组长度.
	//@param :[out] szOutBuf 输出的内存数组.必须在外部申请,长度必须大于输入长度1/2.	
	static bool AsciiToHex(const unsigned char *szInBuf,int nInBufLength,unsigned char *szOutBuf);

	
    static string UTF8ToGB(const string& strUTF8);
	
    static string GBToUTF8(const string& strGb);
	
    static int	  StrtoInt(const string& strNum);
    static string     IntToString(int nNum);
	static string DoubleToString(double dNum);	
    static string Format(const char *_format, ...);
	
    static int already_running(const char *filename,const char *appname);
    static string RemoveQuotes(const char* lpString);
	static int setSysteTime(const time_t& t);
    static void Log(const char *_format, ...);
    static void PrintLogBase(const char* csPath,const char* pFmt, ... );
    static void PrintfMsg(const char *pFmt,...);
	

private:
	static int code_convert(const char* from_charset,const char* to_charset, char* inbuf,
                               size_t inlen, char* outbuf, size_t outlen);
	static char* u2g(char *inbuf);
	static char* g2u(char *inbuf);
    
    static bool IsDir(const char *path);
    static bool IsFile(const char *path);
    static bool IsSpecialDir(const char *path);
    static void GetFileFullPath(const char *path, const char *file_name,  char *file_path);
    static string GetFilePath(const char *szFile);
    

};


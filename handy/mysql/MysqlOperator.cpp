#include "stdio.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "Utility.h"
#ifndef UINT
#define UINT unsigned int
#endif
#include "MysqlOperator.h"

#define TRACE printf
#ifndef HY_LOG_INFO
#define HY_LOG_INFO(x) TRACE("%s:%d %s",__FILE__,__LINE__,x.c_str())
#endif

CMysqlOperator::CMysqlOperator(void)
{
	m_pSqlCon=NULL;
	_bConn = false;
    string strTempMsg;
    strTempMsg.append(__FILE__);
//    strTempMsg.append(__LINE__);
#ifdef USE_SQLITE_DB
	m_pSqliteDb=NULL;
#endif
}

CMysqlOperator::~CMysqlOperator(void)
{
	Close();
    //printf("before mysql_server_end() \n");
    //mysql_server_end(); //有时有问题 ???
    //printf("after  mysql_server_end() \n");
}

bool CMysqlOperator::Connect(const char * szServer,const int nPort,const char * szUser,const char *szPsd,const char *szDataBase)
{
	if (true==_bConn)
		return _bConn;
#ifdef USE_SQLITE_DB

	char *zErrMsg = 0;
	int rc;

	if(m_pSqliteDb)
		return TRUE;
	//打开指定的数据库文件,如果不存在将创建一个同名的数据库文件
	rc = sqlite3_open(szDataBase, &m_pSqliteDb);
	if( rc )
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(m_pSqliteDb));
		sqlite3_close(m_pSqliteDb);
		m_pSqliteDb = NULL;
		return FALSE;
	}
	else 
	{
		//printf("You have opened a sqlite3 database named %s successfully!\nCongratulations! Have fun ! ^-^ \n",szDataBase);
		return TRUE;
	}

#else
	try 
	{  
		m_pSqlCon=mysql_init(NULL);  		
		// localhost:服务器 root为账号密码 test为数据库名 atoi(theApp.m_csMySqlPort)为端口  
		if(!mysql_real_connect(m_pSqlCon, szServer,szUser,szPsd,szDataBase,nPort,NULL,0))  
		{  
			_bConn = false;
            return false;
		}   
		mysql_query(m_pSqlCon, "set character set utf8");
		_bConn = true;
        return true;
	}  
	catch (...)  
	{  
        HY_LOG_INFO(CUtility::Format("connect error\n"));
        return false;
	} 
#endif
}

void CMysqlOperator::Close()
{	
#ifdef USE_SQLITE_DB
	if(m_pSqliteDb)
	{
		sqlite3_close(m_pSqliteDb); //关闭数据库
		m_pSqliteDb = NULL;
	}
#else
	if(m_pSqlCon)
	{
		mysql_close(m_pSqlCon);
		_bConn = false;
		m_pSqlCon=NULL;
        //TRACE("mysql close\n");
	}
#endif	
}

const char* CMysqlOperator::GetErrorString()
{
	return mysql_error(m_pSqlCon);
}

bool CMysqlOperator::Execute(const char * szSqlCmd)
{
#ifdef USE_SQLITE_DB
	if(m_pSqliteDb)
	{
		 char* errmsg;
		 int rc = sqlite3_exec(
			m_pSqliteDb,                                  /* An open database */
			szSqlCmd,                           /* SQL to be evaluated */
			NULL,  /* Callback function */
			NULL,
			&errmsg                              /* Error msg written here */
			);
	}
	return true;
#else
		if(mysql_real_query(m_pSqlCon,szSqlCmd,(UINT)strlen(szSqlCmd))!=0)  
		{   
			const char* pCh = mysql_error(m_pSqlCon);  
            TRACE("execute error=%s\n,cmd=%s\n",pCh,szSqlCmd);
			return false;  
		}
		return true;
#endif
}

bool CMysqlOperator::Execute(const char * szSqlCmd,int *npRows)
{
#ifdef USE_SQLITE_DB
	if(m_pSqliteDb)
	{
		 char* errmsg;
		 int rc = sqlite3_exec(
			m_pSqliteDb,                                  /* An open database */
			szSqlCmd,                           /* SQL to be evaluated */
			NULL,  /* Callback function */
			NULL,
			&errmsg                              /* Error msg written here */
			);
	}
	return true;
#else
	if(mysql_real_query(m_pSqlCon,szSqlCmd,(UINT)strlen(szSqlCmd))!=0)  
	{   
		const char* pCh = mysql_error(m_pSqlCon);  
        TRACE("execute error=%s\ncmd=%s\n",pCh,szSqlCmd);
        return false;
	}

	*npRows=mysql_affected_rows(m_pSqlCon);
	return true;
#endif
}

bool CMysqlOperator::ExecuteWithPhoto(const char * szSqlCmd,...)
{
#ifdef USE_SQLITE_DB
	 //插入二进制数据
	if(m_pSqliteDb)
	{
		// query blob.
        //value = sqlite3_column_blob(pstmt, 1);
        //len = sqlite3_column_bytes(pstmt,1 );

		sqlite3_stmt *pStmt;
		int result = sqlite3_prepare_v2(m_pSqliteDb, szSqlCmd, -1, &pStmt, NULL);
		if(result==SQLITE_OK)//成功插入二进制数据
		{
			va_list arg_ptr; 
			va_start(arg_ptr,szSqlCmd);
			string strCmd=szSqlCmd;
#if 1
			const char  *pszContent=va_arg(arg_ptr,const char *);
			unsigned long nLength=va_arg(arg_ptr,unsigned long);
			int nParamIndex = sqlite3_bind_parameter_index(pStmt, "?");
			sqlite3_bind_blob(pStmt, nParamIndex,pszContent,nLength, NULL);  //记住索引值是从1（而不是0）开始的。
#else
//			int nArgs=0;
			int nFindIndex=0;
			int nParamIndex = 1;
			do
			{
				nFindIndex=strCmd.find("?",nFindIndex);
				if(nFindIndex>=0)
				{
//					nArgs++;
					nFindIndex+=1;
				
					const char  *pszContent=va_arg(arg_ptr,const char *);
					unsigned long nLength=va_arg(arg_ptr,unsigned long);
					//nParamIndex = sqlite3_bind_parameter_index(stmt, "?");
					sqlite3_bind_blob(pStmt, nParamIndex,pszContent,nLength, NULL);  //记住索引值是从1（而不是0）开始的。
					nParamIndex ++;
				}
			}while(nFindIndex>0);
#endif
			//sqlite3_bind_blob(pStmt, index,pszContent,nLength,SQLITE_STATIC);
			va_end(arg_ptr);
		}
		sqlite3_finalize(pStmt);//把刚才分配的内容析构掉
		return true;
	}
	return false;
#else

	//ASSERT(szSqlCmd&&szPhontContent);
    bool bExecute=false;

	string strCmd=szSqlCmd;
	int nArgs=0;
	int nFindIndex=0;
	do
	{
		nFindIndex=strCmd.find("?",nFindIndex);
		if(nFindIndex>=0)
		{
			nArgs++;
			nFindIndex+=1;
		}
	}while(nFindIndex>0);
    //TRACE("args=%d \n",nArgs);
	MYSQL_BIND *pBindParam=new MYSQL_BIND[nArgs];
	unsigned long *parrayLength=new unsigned long [nArgs];
	my_bool *arrayIsNull=new my_bool[nArgs];
	va_list arg_ptr; 
	va_start(arg_ptr,szSqlCmd);
	for(int i=0;i<nArgs;i++)
	{
        const char  *pszContent=va_arg(arg_ptr,const char *);
        unsigned long nLength=va_arg(arg_ptr,unsigned long);
		memset(&pBindParam[i], 0, sizeof(MYSQL_BIND)); 
		if(pszContent)
		{
			pBindParam[i].buffer =(void*) pszContent; 
		}
		else
		{
			//为空要设临时BUF，否则会DOWN
			arrayIsNull[i]=true;
			pBindParam[i].is_null=&arrayIsNull[i];
		}
		
		pBindParam[i].buffer_type = MYSQL_TYPE_LONG_BLOB; 
		pBindParam[i].length=&nLength; 
		parrayLength[i]=nLength;
	} 
	va_end(arg_ptr);
	
	MYSQL_STMT *stmt = mysql_stmt_init(m_pSqlCon); 
	do 
	{
		if (!stmt) 
		{ 
			break; 
		} 
		if (mysql_stmt_prepare(stmt, szSqlCmd, strlen(szSqlCmd))!=0) 
		{ 
			break; 
		} 
		/* Bind the buffers */ 
		if (mysql_stmt_bind_param(stmt, pBindParam)!=0) 
		{  
            TRACE("\n param bind failed");
            TRACE( "\n %s", mysql_stmt_error(stmt));
			break; 
		} 
		/* Supply data in chunks to server */ 
		for(int i=0;i<nArgs;i++)
		{
			unsigned long  nSend=1024*10;
			unsigned long nReadSend=0;
			unsigned long nNeedRead=parrayLength[i];
			char *pData=(char *)pBindParam[i].buffer;
			if(nNeedRead>0)
			{
				do 
				{
					if(nNeedRead<=nSend)
					{
						nSend=nNeedRead;
					}
					if (mysql_stmt_send_long_data(stmt,i, pData+nReadSend, nSend)!=0) 
					{ 
                        TRACE("\n send_long_data failed");
                        TRACE( "\n %s", mysql_stmt_error(stmt));
						break; 
					} 
					nReadSend+=nSend;
					nNeedRead-=nSend;
				} while (nReadSend<parrayLength[i]);
			}

		}
		/* Now, execute the query */ 
		if (mysql_stmt_execute(stmt)!=0) 
		{ 
            TRACE("\n mysql_stmt_execute failed");
			
			break; 
		} 
        bExecute=true;
    } while (false);
	if(!bExecute)
	{
        HY_LOG_INFO(CUtility::Format( "\n %s", mysql_stmt_error(stmt)));
	}
	if(stmt)
	{
		mysql_stmt_close(stmt);
	}
	delete []pBindParam;
	delete []parrayLength;
	delete[] arrayIsNull;
	return bExecute;

#endif
}

int  CMysqlOperator::SelectDataToVector(const char *szSqlCmd,DataRows& vecRows)
{
#ifdef USE_SQLITE_DB
	if(m_pSqliteDb)
	{
		sqlite3_stmt *pStmt;
		int result = sqlite3_prepare_v2(m_pSqliteDb, szSqlCmd, -1, &pStmt, NULL);

		while (sqlite3_step(pStmt) == SQLITE_ROW) 
		{
			int nColCount = sqlite3_data_count(pStmt);
			SRowResult tempResult;
			for(int col = 0; col < nColCount;col ++)
			{
				//				int rowNum = sqlite3_column_int(pStmt, 0);
				char *rowData = (char *)sqlite3_column_text(pStmt, col);

				string strName = sqlite3_column_name(pStmt,col);////pName[col];
				string *pStrTempData = new string;

				pStrTempData->append(rowData);
				tempResult.pStrData.push_back(pStrTempData);
				tempResult.vecDataType.push_back(MYSQL_TYPE_STRING);
			}
			vecRows.rows.push_back(tempResult);
		}
		sqlite3_finalize(pStmt);
	}
	return vecRows.rows.size();
#else
	return SelectDataToMapOrVector(szSqlCmd,&vecRows,NULL);
#endif	
}

int  CMysqlOperator::SelectDataToMapRows(const char *szSqlCmd,SMapRows& mapRows)
{
#ifdef USE_SQLITE_DB
	if(m_pSqliteDb)
	{
		sqlite3_stmt *pStmt;
		int result = sqlite3_prepare_v2(m_pSqliteDb, szSqlCmd, -1, &pStmt, NULL);

		while (sqlite3_step(pStmt) == SQLITE_ROW) 
		{
			SRowResultMap tempResult;
			int nColCount = sqlite3_data_count(pStmt);
			for(int col = 0; col < nColCount;col ++)
			{
				//				int rowNum = sqlite3_column_int(pStmt, 0);
				char *rowData = (char *)sqlite3_column_text(pStmt, col);

				string strName = sqlite3_column_name(pStmt,col);////pName[col];
				string *pStrTempData=new string;

				pStrTempData->append(rowData);
				tempResult.mapRow[strName].pStrData = pStrTempData;
				tempResult.mapRow[strName].eType = MYSQL_TYPE_STRING;
			}
			mapRows.vecRows.push_back(tempResult);
		}
		sqlite3_finalize(pStmt);
	}

	return mapRows.vecRows.size();
#else
	return SelectDataToMapOrVector(szSqlCmd,NULL,&mapRows);
#endif
}

int  CMysqlOperator::SelectDataToMapOrVector(const char *szSqlCmd,DataRows *pVecRows,SMapRows *pMapRows)
{
	SSqlPtr sqlPtr;
	int nRet=SelectData(&sqlPtr,szSqlCmd);
    if(nRet==0)
    {
		for(int nRow=0;nRow<sqlPtr.nRowCount;nRow++)
		{
			int ret = mysql_stmt_fetch(sqlPtr.stmt);
			if (ret!=0 && ret!=MYSQL_DATA_TRUNCATED) 
			{
				break;
			}
			if(pVecRows)
			{
				AddOneDataToVector(&sqlPtr,*pVecRows);
			}
			if(pMapRows)
			{
				AddOneDataToMapRows(&sqlPtr,*pMapRows);
			}
			
		}
	}
	int nRowCount=sqlPtr.nRowCount;
	ReleaseSqlPtr(&sqlPtr);
	return nRowCount;
}

int CMysqlOperator::SelectData(SSqlPtr *pSqlPtr,const char *szSqlCmd)
{
	int ret =0;
	pSqlPtr->stmt = mysql_stmt_init(m_pSqlCon);
	do 
	{
		if(pSqlPtr->stmt==NULL)
		{
			break;
		}
		ret = mysql_stmt_prepare(pSqlPtr->stmt, szSqlCmd, strlen(szSqlCmd));
		if(ret!=0)
		{
			break;
		}
		pSqlPtr->nColCount=mysql_stmt_field_count(pSqlPtr->stmt);
		if(pSqlPtr->nColCount<=0)
		{
			ret=-1;
			break;
		}
		pSqlPtr->pResult=mysql_stmt_result_metadata(pSqlPtr->stmt);
		if(pSqlPtr->pResult==NULL)
		{
			ret=-1;
			break;
		}
		//init results
		pSqlPtr->resultSet=new MYSQL_BIND[pSqlPtr->nColCount];
		pSqlPtr->totalLengthSet=new unsigned long[pSqlPtr->nColCount];	
		pSqlPtr->arrayTempBuf = new char *[pSqlPtr->nColCount]; 
		for(int i=0;i<pSqlPtr->nColCount;++i) 
		{
			pSqlPtr->arrayTempBuf[i] = new char[1024]; 
			memset(pSqlPtr->arrayTempBuf[i],0,1024);
		}

		for(int i=0;i<pSqlPtr->nColCount;i++)
		{
			enum_field_types type=MYSQL_TYPE_LONG_BLOB;
			MYSQL_FIELD *pField= mysql_fetch_field(pSqlPtr->pResult); 
			if(pField!=NULL)
			{
				type=pField->type;
			}
			//special modify for datetime
			if(type==MYSQL_TYPE_DATETIME)
			{
				type=MYSQL_TYPE_VAR_STRING;
			}
			if(type == MYSQL_TYPE_DATE)
			{
				type = MYSQL_TYPE_VAR_STRING;
			}
			if(type == MYSQL_TYPE_TINY)
			{
				type = MYSQL_TYPE_LONG;
			}
			pSqlPtr->totalLengthSet[i]=0;
			memset(&pSqlPtr->resultSet[i], 0, sizeof(MYSQL_BIND)); 
			pSqlPtr->resultSet[i].buffer_type =type;
			pSqlPtr->resultSet[i].length = &pSqlPtr->totalLengthSet[i];
			pSqlPtr->resultSet[i].buffer=pSqlPtr->arrayTempBuf[i];
		}

		ret = mysql_stmt_bind_result(pSqlPtr->stmt, pSqlPtr->resultSet);
		if(ret!=0)
		{
			break;
		}
		ret = mysql_stmt_execute(pSqlPtr->stmt);
		if(ret!=0)
		{
			break;
		}
		ret = mysql_stmt_store_result(pSqlPtr->stmt);
		if(ret!=0)
		{
			break;
		}
		pSqlPtr->nRowCount=(int)mysql_stmt_num_rows(pSqlPtr->stmt);
	}while(false);

	return ret;
}

void CMysqlOperator::ReleaseSqlPtr(SSqlPtr *pSqlPtr)
{
	if(!pSqlPtr)
	{
		return;
	}

    if (pSqlPtr->pResult) //避免内存泄漏
        mysql_free_result(pSqlPtr->pResult);

	if(pSqlPtr->stmt)
	{
		mysql_stmt_free_result(pSqlPtr->stmt);
		mysql_stmt_close(pSqlPtr->stmt);
	}
	delete []pSqlPtr->totalLengthSet;
	delete []pSqlPtr->resultSet;

	for(int i=0;i<pSqlPtr->nColCount;++i) 
	{
		delete[] pSqlPtr->arrayTempBuf[i]; 
	}
	delete[] pSqlPtr->arrayTempBuf;
}

void CMysqlOperator::AddOneDataToVector(SSqlPtr *pSqlPtr,DataRows& vecRows)
{
	SRowResult tempResult;
	for(int i=0;i<pSqlPtr->nColCount;i++)
	{
		string *pStrTempData=new string;
		int nTotalLength=pSqlPtr->totalLengthSet[i];
		if(nTotalLength>0)
		{
			char *pBuf=new char[nTotalLength];
			memset(pBuf,0,nTotalLength);
//            TRACE2(("total_length=%lu\n", nTotalLength));
			int nReadOnce=1024*10;
			int nReadAlready=0;
			int nNeedRead=nTotalLength;
			int ret=0;
			do 
			{
				if(nNeedRead<=nReadOnce)
				{
					nReadOnce=nNeedRead;
				}
				pSqlPtr->resultSet[i].buffer = (pBuf+nReadAlready);
				pSqlPtr->resultSet[i].buffer_length = nReadOnce;  
				ret = mysql_stmt_fetch_column(pSqlPtr->stmt, &pSqlPtr->resultSet[i], i, nReadAlready);
				if (ret!=0)
				{
					break;
				}
				nReadAlready+=nReadOnce;
				nNeedRead-=nReadOnce;
			} while (nReadAlready<nTotalLength);	
			pStrTempData->append((const char *)pBuf,*pSqlPtr->resultSet[i].length);
			delete pBuf;
		}
		tempResult.pStrData.push_back(pStrTempData);
		tempResult.vecDataType.push_back(pSqlPtr->resultSet[i].buffer_type);
	}
	vecRows.rows.push_back(tempResult);
}

void CMysqlOperator::AddOneDataToMapRows(SSqlPtr *pSqlPtr,SMapRows& mapRows)
{
	SRowResultMap tempResult;
	for(int i=0;i<pSqlPtr->nColCount;i++)
	{
		string *pStrTempData=new string;
		int nTotalLength=pSqlPtr->totalLengthSet[i];
		if(nTotalLength>0)
		{
			char *pBuf=new char[nTotalLength];
			memset(pBuf,0,nTotalLength);
//			TRACE("total_length=%lu\n", nTotalLength);
			int nReadOnce=1024*10;
			int nReadAlready=0;
			int nNeedRead=nTotalLength;
			int ret=0;
			do 
			{
				if(nNeedRead<=nReadOnce)
				{
					nReadOnce=nNeedRead;
				}
				pSqlPtr->resultSet[i].buffer = (pBuf+nReadAlready);
				pSqlPtr->resultSet[i].buffer_length = nReadOnce;  
				ret = mysql_stmt_fetch_column(pSqlPtr->stmt, &pSqlPtr->resultSet[i], i, nReadAlready);
				if (ret!=0)
				{
					break;
				}
				nReadAlready+=nReadOnce;
				nNeedRead-=nReadOnce;
			} while (nReadAlready<nTotalLength);	
			pStrTempData->append((const char *)pBuf,*pSqlPtr->resultSet[i].length);
            delete []pBuf;
		}
		MYSQL_FIELD *pField= mysql_fetch_field_direct(pSqlPtr->pResult,i); 
		if(pField!=NULL)
		{
			string strName=pField->name;
			tempResult.mapRow[strName].pStrData=pStrTempData;
			tempResult.mapRow[strName].eType=pSqlPtr->resultSet[i].buffer_type;
		}
	}
	mapRows.vecRows.push_back(tempResult);
}

int  CMysqlOperator::SelectAll(const char *szIp,const char *szPort,const char *szDatabase,const char *szTable,SMapRows& mapRows)
{
	
	int nRows=0;
	if(Connect(szIp,atoi(szPort),"root","admin",szDatabase))
	{
        string strSql = "SELECT * FROM ";
        strSql.append(szTable);
        nRows=SelectDataToMapRows(strSql.c_str(),mapRows);
		Close();
	}
	return nRows;
}

int  CMysqlOperator::Select(const char *szIp, const char *szPort, const char *usr, const char *pwd,const char *szDatabase, const char *szSelectSql, SMapRows& mapRows)
{

	int nRows=0;
	if (Connect(szIp, atoi(szPort), usr, pwd, szDatabase))
	{
		nRows=SelectDataToMapRows(szSelectSql,mapRows);
		Close();
	}
	return nRows;
}

bool CMysqlOperator::TableHasColumn(const char *szDbName,const char *szTable,const char *Column)
{
	SMapRows mapRows;
	string csSql = CUtility::Format("SELECT COUNT(*) FROM information_schema.columns WHERE table_schema='%s' AND table_name='%s' AND column_name='%s';",szDbName,szTable,Column);
	int nRet = SelectDataToMapRows(csSql.c_str(),mapRows);
	if(nRet <= 0)
	{
		return false;
	}
	nRet = mapRows.GetColumn(0,"COUNT(*)")->GetIntData();
	return (nRet > 0);
}

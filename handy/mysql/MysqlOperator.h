#pragma once
#include "mysql.h"
#include <vector>
#include <map>
#include <string>
using namespace std;
#ifdef USE_SQLITE_DB
#include <sqlite3.h>
#endif
//old
struct SRowResult
{
	vector<string*>  pStrData;//列数据
	vector<enum_field_types> vecDataType;
};
struct DataRows
{
	vector<SRowResult> rows;
	SRowResult operator[](int i)
	{
		return rows[i];
	}
    bool IsEmpty()
	{
		if (!rows.empty())
		{
			return false;
		}
		return true;
	}
	~DataRows()
	{
		int nRows=rows.size();
		for(int i=0;i<nRows;i++)
		{
			int nSize=rows[i].pStrData.size();
			for(int j=0;j<nSize;j++)
			if(rows[i].pStrData[j])
			{
				delete rows[i].pStrData[j];
				rows[i].pStrData[j]=NULL;
			}
		}
	}
}; 


//map
struct SColumn
{
	string *pStrData;
	enum_field_types eType;
	int GetIntData()
	{
		int nResult = 0;
		if(eType == MYSQL_TYPE_LONG || eType == MYSQL_TYPE_LONGLONG)
		{
			if(!pStrData->empty())
			{
				nResult = *(int*)(pStrData->c_str());
			}
		}
		return nResult;
	}

	//将int 值转为string
	string GetIntString()
	{
		char strValue[32];
		sprintf(strValue,"%d",GetIntData());
		return strValue;
	}

	double GetDoubleData()
	{
		double dResult = 0.0;
		if(eType == MYSQL_TYPE_FLOAT || eType == MYSQL_TYPE_DOUBLE)
		{
			if(!pStrData->empty())
			{
				dResult = *(double*)(pStrData->c_str());
			}
		}
		return dResult;
	}
};
struct SRowResultMap
{
	map<string,SColumn> mapRow;
	void Copy(SRowResultMap result)
	{
		map<string,SColumn>::iterator it;
		for(it=result.mapRow.begin();it!=result.mapRow.end();it++)
		{
			SColumn column;
			column.eType=it->second.eType;
			column.pStrData=new string;
			column.pStrData->append(it->second.pStrData->c_str(),it->second.pStrData->length());
			mapRow[it->first]=column;
		}
	}
};

//只能局部使用或者传参引用,不能直接传值,因为会释放内部申请的pStrData;
struct SMapRows
{
	vector<SRowResultMap> vecRows;
	SRowResultMap operator[](int i)
	{
		return vecRows[i];
	}
    bool IsEmpty()
	{
		if (!vecRows.empty())
		{
			return false;
		}
		return true;
	} 	
	~SMapRows()
	{
		Clear();
	}

	SColumn * GetColumn(int i,const string& strName)
	{
		if(vecRows.size()<=(unsigned int )i)
		{
			return NULL;
		}
		return &vecRows[i].mapRow[strName];
	}
	SRowResultMap *GetRow(int i)
	{
		if(vecRows.size()<=(unsigned int )i)
		{
			return NULL;
		}
		return &vecRows[i];
	}
	void Clear()
	{
		int nRows=vecRows.size();
		for(int i=0;i<nRows;i++)
		{
			map<string,SColumn>::iterator it;
			for(it=vecRows[i].mapRow.begin();it!=vecRows[i].mapRow.end();it++)
			{
				if(it->second.pStrData)
				{
					delete it->second.pStrData;
					it->second.pStrData=NULL;
				}
			}
			vecRows[i].mapRow.clear();
		}
		vecRows.clear();
	}

	void Delete(int i)
	{
		map<string,SColumn>::iterator it;
		for(it=vecRows[i].mapRow.begin();it!=vecRows[i].mapRow.end();it++)
		{
			if(it->second.pStrData)
			{
				delete it->second.pStrData;
				it->second.pStrData=NULL;
			}
		}
		vecRows[i].mapRow.clear();
	}
}; 

struct SSqlPtr
{
	char **arrayTempBuf ;
	MYSQL_BIND *resultSet;
	unsigned long *totalLengthSet;
	MYSQL_RES *pResult;
	MYSQL_STMT* stmt ;
	int      nRowCount;
	int		 nColCount;
	SSqlPtr()
	{
		arrayTempBuf = NULL;
		resultSet=NULL;
		totalLengthSet=NULL;
		pResult=NULL;
		stmt=NULL ;
		pResult=NULL;
		nRowCount=0;
		nColCount=0;
	}
};


class CMysqlOperator
{
public:
	CMysqlOperator(void);
	~CMysqlOperator(void);
public:
    bool Connect(const char * szServer,const int nPort,
		        const char * szUser,const char *szPsd,const char *szDataBase);
	void Close();

	//执行普通的SQL语句
    bool Execute(const char * szSqlCmd);
    bool Execute(const char * szSqlCmd,int *npRows);

	/*
	* 有图片的执行
	* @param szSqlCmd:			执行语句
	* @param ...				多参数,参数方式:const char *szPhontContent,unsigned long  nPhotoLength,重复
	* @return					执行是否成功
	* TEST:pass
	*/	
    bool ExecuteWithPhoto(const char * szSqlCmd,...);
	int  SelectDataToVector(const char *szSqlCmd,DataRows& vecRows);
	int  SelectDataToMapRows(const char *szSqlCmd,SMapRows& mapRows);

	int  SelectAll(const char *szIp,const char *szPort,const char *szDatabase,const char *szTable,SMapRows& mapRows);
	int  Select(const char *szIp, const char *szPort, const char *usr, const char *pwd, const char *szDatabase, const char *szSelectSql, SMapRows& mapRows);

	const char* GetErrorString();
	bool TableHasColumn(const char *szDbName,const char *szTable,const char *Column);

private:
	int  SelectDataToMapOrVector(const char *szSqlCmd,DataRows *pVecRows,SMapRows *pMapRows);
	void AddOneDataToVector(SSqlPtr *pSqlPtr,DataRows& vecRows);
	void AddOneDataToMapRows(SSqlPtr *pSqlPtr,SMapRows& mapRows);
	int  SelectData(SSqlPtr *pSqlPtr,const char *szSqlCmd);
	void ReleaseSqlPtr(SSqlPtr *pSqlPtr);

private:
	MYSQL *m_pSqlCon;
	bool _bConn;
#ifdef USE_SQLITE_DB
	sqlite3* m_pSqliteDb;
#endif

};

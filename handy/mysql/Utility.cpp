 
#include "Utility.h"
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <iconv.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <sys/vfs.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/hdreg.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <net/if.h>

//#include <unistd.h>
#include <fcntl.h>
//#include <printf.h>
#include <string.h>
#include <errno.h>
//#include <sys/stat.h>

using namespace std;
#define TIMEOUT_SEC 20
CUtility::CUtility()
{  
 
}

CUtility::~CUtility()
{

}

//取得自1970年1月1日0时以来的毫秒数
INT64  CUtility::GetTickTime()
{
    struct timeval tv;
    if (gettimeofday(&tv,NULL) != 0)
        return 0;

	long int t1 = tv.tv_sec;
    t1 *= 1000;
    t1 += (tv.tv_usec / 1000);
    return t1;
}

unsigned int CUtility::GetTickCountOld()
{
    struct timeval tv;
        if(gettimeofday(&tv,NULL)!=0)
        {
         return 0;
        }
        return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);

}

string CUtility::GetExeFilePath()
{
    const int MAX_PATH = 260;
    string strPath = "";
    char szWorkDir[MAX_PATH] = {0} ;
    if(getcwd(szWorkDir, MAX_PATH))
    {
        strPath.append(szWorkDir).append("/");
    }
    return strPath;
}

int CUtility::GetFileSize(const char *szFileName)
{
	int nSize=0;
	struct stat buffer;
	int         status;
	status = stat(szFileName, &buffer);
	if(status>=0)
	{
		nSize=buffer.st_size;
														
	}
	return nSize;


}

bool CUtility::IsFileExist(const char *szFilePath)
{
	int nSize=GetFileSize(szFilePath); 
	return (nSize>0)?true:false;
	
}


bool CUtility::WriteStrToFile(const char * pszContent,int nLength,const char *szName ,bool bAppend)
{
	if(!pszContent||nLength<=0)
	{
		return false;

	}
	FILE * pFile;

	const char * szLogPath=szName;
	if(bAppend)
	{
		pFile = fopen( szLogPath, "ab" );
	}
	else
	{
		pFile = fopen( szLogPath, "wb+" );
	}
	
	if ( pFile != NULL )
	{
		fwrite(pszContent,sizeof(char)*nLength,1,pFile);
		fclose(pFile);
		return true;
	}
	return false;
}
bool CUtility::WriteStrToFile(FILE * pFile, const char * pszContent,int nLength)
{
    if(!pszContent||nLength<=0)
    {
        return false;
    }
    if ( pFile != NULL )
    {
        fwrite(pszContent,sizeof(char)*nLength,1,pFile);
        return true;
    }
    return false;
}


 int CUtility::ReadStrFromFile( char * pszContent,const char *szName,int nLength)
{
	int nReadLength=nLength;
	if(nReadLength==0)
	{
		nReadLength=GetFileSize(szName);
	}
	if(nReadLength>0)
	{
		FILE * pFile=fopen(szName, "rb" );
		if ( pFile != NULL )
		{
			fread(pszContent,nReadLength,1,pFile);
			fclose(pFile);
			return nReadLength;
		}
	}
	return 0;
}

string CUtility::ReadStrFromFile(const char *szName)
{
    string strContext;
    int nReadLength = GetFileSize(szName);
    if(nReadLength>0)
    {
        FILE * pFile=fopen(szName, "rb" );
        if ( pFile != NULL )
        {
            char* pBuffer = (char*) malloc(nReadLength+1);
            memset(pBuffer,0,nReadLength+1);
            fread(pBuffer,nReadLength,1,pFile);
            fclose(pFile);
            strContext = string(pBuffer,nReadLength);
            free(pBuffer);
        }
    }
    return strContext;
}

string CUtility::GetCurHostIP()
{
    int i;
    int sockfd;
    string strIP;
    struct ifconf ifconf;
    unsigned char buf[512];
    struct ifreq *ifreq;    //初始化ifconf
    ifconf.ifc_len = 512;
    ifconf.ifc_buf = (char*)buf;
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
    {
        perror("socket\n");
        return 0;
     }
     ioctl(sockfd, SIOCGIFCONF, &ifconf);    //获取所有接口信息

    //接下来一个一个的获取IP地址
    ifreq = (struct ifreq*)buf;
    for(i=(ifconf.ifc_len/sizeof(struct ifreq)); i>0; i--)
    {
        if(ifreq->ifr_flags == AF_INET && strcmp(ifreq->ifr_name,"lo"))
        {
            //for ipv4
//          CUtility_Printf("name = [%s]\n", ifreq->ifr_name);
//          if (ioctl(sockfd, SIOCGIFADDR, ifreq) >= 0)
            strIP = inet_ntoa(((struct sockaddr_in*)&(ifreq->ifr_addr))->sin_addr);
            break;
        }
        ifreq++;
     }

    close(sockfd);
    return strIP;
}

 //磁盘相关
int CUtility::GetDiskFreeSpace(const char *szPath)
{
    struct statfs diskInfo;
    if(szPath==NULL)
    {
        statfs("./",&diskInfo);
    }
    else
    {
        string strPath=GetFilePath(szPath);
        statfs(strPath.c_str(),&diskInfo);
    }
    unsigned long long lBlockSize=diskInfo.f_bsize;
    unsigned long long lFreeSize=lBlockSize*diskInfo.f_bfree;
    unsigned long long lSizeInMB=lFreeSize/(1024*1024);
    return (int)lSizeInMB;
}

 //磁盘相关
int CUtility::GetDiskTotalSpace(const char *szPath)
{
    struct statfs diskInfo;
    if(szPath==NULL)
    {
        statfs("./",&diskInfo);
    }
    else
    {
       string strPath=GetFilePath(szPath);
       statfs(strPath.c_str(),&diskInfo);
    }
    unsigned long long lBlockSize=diskInfo.f_bsize;
    unsigned long long lFreeSize=lBlockSize*diskInfo.f_blocks;
    unsigned long long lSizeInMB=lFreeSize/(1024*1024);
    return (int)lSizeInMB;
}

int CUtility::CreateFolder(const char* strPath,bool isFile)
{
	char   DirName[256];   
	strcpy(DirName,   strPath);   
	int   i,len   =   strlen(DirName);   
	if(!isFile)
	{
		if(DirName[len-1]!='/')   
		{
			strcat(DirName,   "/");   
			len   =   strlen(DirName);   
		}
	}
	    
	for(i=1;   i<len;   i++)   
	{   
		if(DirName[i]=='/')   
		{   
			DirName[i]   =   0;   
			if(   access(DirName,   0)!=0   )   
			{   
				if(mkdir(DirName,   0755)==-1)   
				{   
                    //perror("mkdir   error"); //注释掉是为了避免对普通用户造成困扰
					return   -1;   
				}   
			}   
			DirName[i]   =   '/';   
		}   
	}   
  return   0;
}

void CUtility::DeleteFile(const char *path,bool bDelEmptyFolder)
{
	int nRet=remove(path);
    CUtility_Printf("Delete file:%s,ret=%d\n",path,nRet);
	int nRetRmDir=-1;
	if(bDelEmptyFolder)
	{
		char   DirName[256];   
		strcpy(DirName,   path);   
		int   i,len   =   strlen(DirName);   
		len   =   strlen(DirName);   
		
		for(i=len-1;   i>0;   i--)   
		{   
			if(DirName[i]=='/')   
			{   
				DirName[i]   =   0;   
				nRetRmDir=rmdir(DirName);
				DirName[i]   =   '/';   
			}   
		}   
	}
}

//是否目录
bool CUtility::IsDir(const char *path)
{
    struct stat statbuf;
    if(lstat(path, &statbuf) ==0)
    {
        return S_ISDIR(statbuf.st_mode) != 0;
    }
    return false;
}

bool CUtility::IsFile(const char *path)
{
    struct stat statbuf;
    if(lstat(path, &statbuf) ==0)
        return S_ISREG(statbuf.st_mode) != 0;
    return false;
}

//是否特殊目录
bool CUtility::IsSpecialDir(const char *path)
{
    return strcmp(path, ".") == 0 || strcmp(path, "..") == 0;
}

//获取路径
void CUtility::GetFileFullPath(const char *path, const char *file_name,  char *file_path)
{
    strcpy(file_path, path);
    if(file_path[strlen(path) - 1] != '/')
        strcat(file_path, "/");
    strcat(file_path, file_name);
}

void CUtility::DeleteFileRecursive(const char *path)
{
    DIR *dir;
    dirent *dir_info;
    char file_path[PATH_MAX];
    if(IsFile(path))
    {
        remove(path);
        return;
    }
    if(IsDir(path))
    {
        if((dir = opendir(path)) == NULL)
            return;
        while((dir_info = readdir(dir)) != NULL)
        {
            GetFileFullPath(path, dir_info->d_name, file_path);
            if(IsSpecialDir(dir_info->d_name))
                continue;
            DeleteFileRecursive(file_path);
            rmdir(file_path);
        }
        rmdir(path);
    }
}

//16进制内存值转16进制字符串
//@param :[in]  szInBuf 输入的内存数组.
//@param :[in]  nInBufLength  内存数组长度.
//@param :[out] szOutBuf 输出的字符串数组内容.必须在外部申请,长度为输入长度的两倍.
//@param :[IN]  bLower 是否输出小写字母;
bool CUtility::HexToAscii(const unsigned char *szInBuf,int nInBufLength,unsigned char *szOutBuf,bool bLower)
{
	if(!szInBuf||!szOutBuf||nInBufLength<=0)
	{
		return false;
	}
	char szBegin='A';
	if (bLower) 
	{
		szBegin='a';
		
	}
	for (int i=0; i<nInBufLength;i++)
	{
		//取内存数组的每个数的高位为低位,分别转化为对应的ASCII字符
		int j=2*i;
		int nHigh=szInBuf[i]>>4;
		if(nHigh>=0&&nHigh<=9)
		{
			szOutBuf[j]='0'+nHigh;
		}
		else
		{
			szOutBuf[j]=szBegin+nHigh-10;			
		}

		int nLow=szInBuf[i]&0xF;
		if(nLow>=0&&nLow<=9)
		{
			szOutBuf[j+1]='0'+nLow;
		}
		else
		{
			szOutBuf[j+1]=szBegin+nLow-10;			
		}
	}
	return true;
	
}
//16进制字符串转16进制内存值
//@param :[in]  szInBuf 输入的字符串数组.
//@param :[in]  nInBufLength  字符串数组长度.
//@param :[out] szOutBuf 输出的内存数组.必须在外部申请,长度必须大于输入长度1/2.	
bool CUtility::AsciiToHex(const unsigned char *szInBuf,int nInBufLength,unsigned char *szOutBuf)
{
	if(!szInBuf||!szOutBuf||nInBufLength<=0)
	{
		return false;
	}
	for (int i=0; i<nInBufLength/2;i++)
	{
		//取字符数组的每个数转化为对应的内存数组值.
		
		//取高位
		if(szInBuf[2*i]>='0'&&szInBuf[2*i]<='9')
		{
			szOutBuf[i]=szInBuf[2*i]-'0';
		}
		else 
		{
			szOutBuf[i]=szInBuf[2*i]-'A'+10;
		}
		//取低位
		if(szInBuf[2*i+1]>='0'&&szInBuf[2*i+1]<='9')
		{
			int nLow=szInBuf[2*i+1]-'0';
            CUtility_Printf("%d\n",nLow);
			szOutBuf[i]=szOutBuf[i]<<4;
			szOutBuf[i]+=nLow;
		}
		else 
		{
			
			int nLow=szInBuf[2*i+1]-'A'+10;
            CUtility_Printf("%d\n",nLow);
			szOutBuf[i]=szOutBuf[i]<<4;
            szOutBuf[i]+=nLow;
		}
	}
	return true;
	
}

//获取DATA,"20110502"
string  CUtility::GetCurDate()
{
	struct tm *p;                                                                                                                          
    time_t timep;                                                                                                                          
    int year = 0;                                                                                                                          
    int mon = 0;                                                                                                                           
    int mday = 0;                                                                                                                          
	
    time( &timep );                                                                                                                        
    p = localtime( &timep );                                                                                                                  
	
    year = 1900 + p->tm_year;                                                                                                              
    mon = 1 + p->tm_mon;                                                                                                                   
    mday = p->tm_mday;                                                                                                                     
	
    char szBuf[100]={0};
    sprintf( szBuf, "%d%02d%02d", year, mon, mday );     
    string strRet=szBuf;
    return strRet;      
}

//获取当前时间,"231112"
string CUtility::GetCurTimeInOneDay()
{
	struct tm *p;                                                                                                                          
    time_t timep;                                                                                                                          
    int nHour = 0;                                                                                                                          
    int nMin = 0;                                                                                                                           
    int nSec = 0;                                                                                                                          
	
    time( &timep ); 
	//GMT国际时间
    //p = gmtime( &timep );                                                                                                                  
	p=localtime(&timep);

    nHour = p->tm_hour;                                                                                                              
    nMin = p->tm_min;                                                                                                                   
    nSec = p->tm_sec;                                                                                                                     
	
    char szBuf[100]={0};
    sprintf( szBuf, "%02d%02d%02d", nHour, nMin, nSec );                                                           
    string strRet=szBuf;
    return strRet;      
}

string CUtility::GetFullTime()
{
	struct tm *p;                                                                                                                          
    time_t timep;                                                                                                                          
    int year = 0;                                                                                                                          
    int mon = 0;                                                                                                                           
    int mday = 0;                                                                                                                          
	int nHour = 0;                                                                                                                          
    int nMin = 0;                                                                                                                           
    int nSec = 0;    
	
    time( &timep );                                                                                                                        
    p = localtime( &timep );                                                                                                                  
	
    year = 1900 + p->tm_year;                                                                                                              
    mon = 1 + p->tm_mon;                                                                                                                   
    mday = p->tm_mday;        
	
	nHour = p->tm_hour;                                                                                                              
    nMin = p->tm_min;                                                                                                                   
    nSec = p->tm_sec;                                                                                                                     
	
	char szBuf[100];
    sprintf( szBuf, "%d-%02d-%02d %02d:%02d:%02d", year, mon, mday,nHour, nMin, nSec ); 
	string strFullTime=szBuf;
	
	return strFullTime;
}


string CUtility::GetFullTimeNoInterVal()
{
	struct tm *p;                                                                                                                          
    time_t timep;                                                                                                                          
    int year = 0;                                                                                                                          
    int mon = 0;                                                                                                                           
    int mday = 0;                                                                                                                          
	int nHour = 0;                                                                                                                          
    int nMin = 0;                                                                                                                           
    int nSec = 0;    
	
    time( &timep );                                                                                                                        
    p = localtime( &timep );                                                                                                                  
	
    year = 1900 + p->tm_year;                                                                                                              
    mon = 1 + p->tm_mon;                                                                                                                   
    mday = p->tm_mday;        
	
	nHour = p->tm_hour;                                                                                                              
    nMin = p->tm_min;                                                                                                                   
    nSec = p->tm_sec;                                                                                                                     
	
	char szBuf[100];
    sprintf( szBuf, "%d%02d%02d%02d%02d%02d", year, mon, mday,nHour, nMin, nSec ); 
	string strFullTime=szBuf;
	
	return strFullTime;
}

long CUtility::GetCurrentTimeSeconds()
{
    struct timeval tv;
    if(gettimeofday(&tv,NULL) != 0)
    {
        return 0L;
    }
    return tv.tv_sec;
}

string CUtility::GetCurrentTimeSecondsString()
{
    long seconds = GetCurrentTimeSeconds();
    char szSec[15] = {0};
    sprintf(szSec,"%ld",seconds);
    string strSec = szSec;
    return strSec;
}

string CUtility::GetCurrentFullTimeInLinux()
{
    struct tm *p;
    time_t timep;
    int year = 0;
    int mon = 0;
    int mday = 0;
    int nHour = 0;
    int nMin = 0;
    int nSec = 0;

    time( &timep );
    p = localtime( &timep );

    year = 1900 + p->tm_year;
    mon = 1 + p->tm_mon;
    mday = p->tm_mday;

    nHour = p->tm_hour;
    nMin = p->tm_min;
    nSec = p->tm_sec;

    char szBuf[100];
    sprintf(szBuf,"%02d%02d%02d%02d%d.%02d",mon,mday,nHour,nMin,year,nSec);
    string strFullTime=szBuf;

    return strFullTime;
}

string CUtility::GetTimeByTimeT(time_t timeP)
{
    struct tm *p;
    int year = 0;
    int mon = 0;
    int mday = 0;
    int nHour = 0;
    int nMin = 0;
    int nSec = 0;


    p = localtime( &timeP );

    year = 1900 + p->tm_year;
    mon = 1 + p->tm_mon;
    mday = p->tm_mday;

    nHour = p->tm_hour;
    nMin = p->tm_min;
    nSec = p->tm_sec;

    char szBuf[100];
    sprintf( szBuf, "%d-%02d-%02d %02d:%02d:%02d", year, mon, mday,nHour, nMin, nSec );
    string strFullTime=szBuf;

    return strFullTime;
}


void CUtility::ListDirInPath(const char *path, vector<FilePathInfo> &vecDir) 
{
	struct dirent* ent = NULL;
	DIR *pDir;
	pDir = opendir(path);
	if (pDir == NULL) {
		//被当作目录，但是执行opendir后发现又不是目录，比如软链接就会发生这样的情况。
		return;
	}
	while (NULL != (ent = readdir(pDir)))
	{
		if (ent->d_type == DT_DIR) 
		{
			if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) 
			{
				continue;
			}
			//directory
			std::string strpath(path);
			std::string strdirName(ent->d_name);
            std::string fullDirPath = strpath + strdirName;
			
			FilePathInfo info;
			info.m_strName=strdirName;
			info.m_strFullPath=fullDirPath;
			vecDir.push_back(info);			
		}
		
	}
	closedir(pDir);
}

void CUtility::ListFileInPath(const char *path, map<string ,FilePathInfo> &mapDir,const char *szExt)
{
	struct dirent* ent = NULL;
	DIR *pDir;
	pDir = opendir(path);
	if (pDir == NULL) {
		//被当作目录，但是执行opendir后发现又不是目录，比如软链接就会发生这样的情况。
		return;
	}
	while (NULL != (ent = readdir(pDir)))
	{
		if (ent->d_type == DT_REG) 
		{
			
			//file
			std::string strDirName(ent->d_name);
			if(szExt)
			{
				int nFind=strDirName.find(szExt);
				if(nFind<0)
				{
					continue;
				}
									  
			}
			std::string strpath(path);
			std::string fullDirPath = strpath + "/" + strDirName;
			
			FilePathInfo info;
			info.m_strName=strDirName;
			info.m_strFullPath=fullDirPath;
			mapDir[strDirName]=info;			
		}
		
	}
	closedir(pDir);

}


void CUtility::PrintfMsg(const char *pFmt,...)
{
#if KEYBOARD_INPUT_ENABLE
    string strTime=GetFullTime();
    printf("%s    ",strTime.c_str());

    va_list arg;
    va_start( arg, pFmt );
    vprintf(pFmt,arg);
    va_end( arg );
#else
    va_list arg;
    va_start( arg, pFmt );

    char szBuffer[256];
    int nLength = sizeof(szBuffer);
    memset(szBuffer, 0x00, nLength);

    vsnprintf(szBuffer, nLength, pFmt, arg);
    va_end( arg );

    CUtility_Printf(szBuffer);
#endif
}



int CUtility::code_convert(const char* from_charset,const char* to_charset, char* inbuf,
                 size_t inlen, char* outbuf, size_t outlen)
{
	iconv_t cd; 
	char** pin = &inbuf;    
	char** pout = &outbuf;
	cd = iconv_open(to_charset,from_charset);   
	if(cd == 0)
		return -1;
	memset(outbuf,0,outlen);  
	//(iconv_t cd, const char* * inbuf, size_t *inbytesleft, char* * outbuf, size_t *outbytesleft);
    if(iconv(cd,pin,( size_t*)&inlen,pout,(size_t*)&outlen)  == -1)//in 64 bit,size_t != int
    {
        iconv_close(cd);
        return -1;
    }
	iconv_close(cd);
	return 0;   
}

//UNICODE码转为GB2312码   
//成功则返回一个动态分配的char*变量，需要在使用完毕后手动free，失败返回NULL

char* CUtility::u2g(char *inbuf)   
{
    int nOutLen = 2 * (strlen(inbuf) +1);
	if(nOutLen <= 0) return NULL;  
	
	char* szOut = new char[nOutLen];
	//char* szOut = (char*)malloc(nOutLen);
	if (-1 == code_convert("utf-8","gb18030",inbuf,strlen(inbuf),szOut,nOutLen))
	{
		delete szOut;
		szOut = NULL;
	}
	return szOut;
}   

//GB2312码转为UNICODE码   
//成功则返回一个动态分配的char*变量，需要在使用完毕后手动free，失败返回NULL

char* CUtility::g2u(char *inbuf)   
{
	
    int nOutLen = 2 * (strlen(inbuf) + 1);
	if(nOutLen<=0)
	{
		return NULL;
	}
	char* szOut = new char[nOutLen];
	if (-1 == code_convert("gb18030","utf-8",inbuf,strlen(inbuf),szOut,nOutLen))
	{
		delete  szOut;
		szOut = NULL;
	}
	return szOut;
}  

string CUtility::UTF8ToGB(const string& strUTF8)
{
	string strGb;
	char * szValue=u2g((char *)strUTF8.c_str() ) ;
	if(szValue)
	{
		strGb.append(szValue);
        delete []szValue;
	}
    return strGb;
}

string CUtility::GBToUTF8(const string& strGb)
{
	string strUTF8;
	char * szValue=g2u((char *)strGb.c_str() ) ;
	if(szValue)
	{
		strUTF8.append(szValue);
        delete [] szValue;
	}
    return strUTF8;	
}

int	  CUtility::StrtoInt(const string& strNum)
{
	return atoi(strNum.c_str());

}

string CUtility::IntToString(int nNum)
{
    char szTemp[20]={0};
    sprintf(szTemp,"%d",nNum);
    string strNum=szTemp;
    return strNum;
}
string CUtility::UintToString(unsigned int nNum)
{
    char szTemp[20]={0};
    sprintf(szTemp,"%u",nNum);
    return string(szTemp);
}

string CUtility::DoubleToString(double dNum)
{
	char szTemp[50]={0};
	sprintf(szTemp,"%.10f",dNum);
	string strNum=szTemp;
	return strNum;
}

void CUtility::GetTimeAndTick(unsigned int &nSec,unsigned int &nMsec)
{
	struct timeval tv;
	if(gettimeofday(&tv,NULL)!=0)
	{
	 return ;
	}
	nSec=tv.tv_sec;
    nMsec=(tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}

bool CUtility::IfNeededChangeTime(const string& strTime,const string& strTimeString)
{
    unsigned int uTime=strtoul(strTime.c_str(),NULL,10);
	if(uTime==0)
	{
		return false;
	}
    //test oml::
    //uTime+=10;
    struct timeval tv;
	if(gettimeofday(&tv,NULL)!=0)
	{
	 return false;
	}
	unsigned int uSecNow=tv.tv_sec;
	int nSpan=abs((int)(uTime-uSecNow));
    bool bFailed=false;
	if(nSpan>=10)
	{
        unsigned int uSecNowAfter;
        struct tm tm_time;
        time_t timeSecond;
        strptime((char *)strTimeString.c_str(),"%Y-%m-%d %H:%M:%S",&tm_time);   
        timeSecond=mktime(&tm_time);
        //比较获取到的两个时间,两个时间不相该差异超过5秒
        int  nSpanTransTime=abs((int)(uTime-timeSecond));
        
        if(nSpanTransTime<5)
        {
            time_t setTime=uTime;
            stime(&setTime);
            
            struct timeval tvAfter;
            gettimeofday(&tvAfter,NULL);
            uSecNowAfter=tvAfter.tv_sec;
            
            int nSpan=abs((int)(uTime-uSecNowAfter));
            if(nSpan>10)
            {
                //设置失败,再设一次
                stime(&setTime);
                bFailed=true;
            }
        }
        //log
        char szTemp[200]={0};
        sprintf(szTemp,"cur time=%d change time=%d ,timeafter=%d,transTime=%s set failed=%d\n",uSecNow,uTime,uSecNowAfter,strTimeString.c_str(),bFailed);
        string strTime=szTemp;
        string strPath="/media/sda1/log.txt";
        strPath="./log.txt";
        int nSize=GetFileSize(strPath.c_str());
        if(nSize>2048*1000)
        {
            remove(strPath.c_str());
        }
        CUtility_Printf("set time:%s",strTime.c_str());
        //CUtility::WriteStrToFile(strTime.c_str(),strTime.length(),strPath.c_str());
	}
    return true;
    
}

string CUtility::GetFilePath(const char *szFile)
{
    if(!szFile)
    {
        return "./";
    }
    string strPath=""; 
    char   szDirName[256];   
    strcpy(szDirName,   szFile);   
    int   nLen=   strlen(szDirName);   
    int   nIndex=0;
    for(nIndex=nLen-1;nIndex>0;nIndex--)   
    {   
        if(szDirName[nIndex]=='/')   
        {   
            break;
        }   
    } 
    strPath.append(szFile,nIndex+1);

    return strPath;
    
}

string CUtility::Format(const char *_format, ...)
{
    string strFormat;

    va_list ap;
    va_start(ap, _format);

    int nLength = 1024;
    nLength++;

    char* szBuffer = (char*)malloc(nLength);
    memset(szBuffer, 0x00, nLength);
    try
    {
        int nRetLength = vsnprintf(szBuffer, nLength, _format, ap);

        //the memory is not enough.
        if(nRetLength > nLength)
        {
            nLength = nRetLength +1;

            //realloc memory.
            free(szBuffer);
            szBuffer = (char*)malloc(nLength);
            memset(szBuffer, 0x00, nLength);

            //can not use ap,error
            va_list ap2;
            va_start(ap2, _format);
            vsnprintf(szBuffer, nLength, _format, ap2);
            va_end(ap2);
        }
    } catch (...)
    {
        CUtility_Printf("ERROR: format the string failed...\n");
        free(szBuffer);
        return "";
    }

    va_end(ap);
    strFormat.append(szBuffer);
    free(szBuffer);
    return strFormat;
}

#define LOCKMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/* set advisory lock on file */
static int lockfile(int fd)
{
        struct flock fl;

        fl.l_type = F_WRLCK;  /* write lock */
        fl.l_start = 0;
        fl.l_whence = SEEK_SET;
        fl.l_len = 0;  //lock the whole file

        return(fcntl(fd, F_SETLK, &fl));
}

int CUtility::already_running(const char *filename,const char *appname)
{
        int fd;
        char buf[16];

        fd = open(filename, O_RDWR | O_CREAT, LOCKMODE);
        if (fd < 0) {
                CUtility_Printf("can't open %s: %m\n", filename);
#if ! KEYBOARD_INPUT_ENABLE
                CUtility_SaveLogFromMemoryToDisk();
#endif
                exit(1);
        }


        /* 先获取文件锁 */
        if (lockfile(fd) == -1) {
                if (errno == EACCES || errno == EAGAIN) {
                        CUtility_Printf("%s already run!\n", appname);
                        close(fd);
                        return 1;
                }
                CUtility_Printf("can't lock %s: %m\n", filename);
#if ! KEYBOARD_INPUT_ENABLE
                CUtility_SaveLogFromMemoryToDisk();
#endif
                exit(1);
        }

        /* 写入运行实例的pid */
        ftruncate(fd, 0);
        sprintf(buf, "%ld", (long)getpid());
        write(fd, buf, strlen(buf) + 1);

        return 0;
}

//去掉引号
string CUtility::RemoveQuotes(const char* lpString)
{
    int len = strlen(lpString);
    string strRes;
    if(*lpString == '"')
        strRes = string(lpString+1,len-2);
    else
        strRes = lpString;
    return strRes;
}

int CUtility::setSysteTime(const time_t& t) 
 { 
#ifdef WIN32
	 SYSTEMTIME st; 
	 memset(&st,0,sizeof(st)); 
	 TimetToSystemTime(t,&st); 
	 if (::SetSystemTime(&st))  
	 { 
		 ::SendMessage(HWND_BROADCAST, WM_TIMECHANGE, 0, 0); 
		 return 0; 
	 } 
	 return -1; 
#else 
	 time_t setTime=t;
	 stime(&setTime);
     return 0;
#endif

 } 

void CUtility::Log(const char *_format, ...)
{
    string strTime = CUtility::GetFullTime();
    strTime.append(" ");

    va_list ap;
    va_start(ap, _format);

    int nLength = 1024;

    char* szBuffer = (char*)malloc(nLength);
    memset(szBuffer, 0x00, nLength);

    strcpy(szBuffer,strTime.c_str());
    vsnprintf(szBuffer + strTime.length(), nLength, _format, ap);
    va_end(ap);
    CUtility_Printf(szBuffer);
    free(szBuffer);
}

void  CUtility::PrintLogBase(const char* csPath,const char* pFmt, ... )
 {
     time_t tm = ::time(NULL);
     string sCurtime = string(::ctime(&tm));

     FILE * pFile;
      const char* csLogPath = csPath;//GetExeFilePath()+_T("LOG.TXT");
     int dSize=CUtility::GetFileSize(csLogPath);

     int dEndSize=1024*1024*20;
     if(dSize>dEndSize)
     {
         DeleteFile(csLogPath,false);
     }

     pFile = fopen( csLogPath, "ab" );
     if ( pFile != NULL )
     {
         va_list arg;
         va_start( arg, pFmt );
         vfprintf( pFile, pFmt, arg );
         va_end( arg );
        // fwrite("\t",1,1,pFile);
        // fwrite(sCurtime.c_str(),sCurtime.length()-1,1,pFile);
         fwrite("\r\n",2,1,pFile);
         fclose( pFile );
     }

 }

bool CUtility::IsDirExist(const char* szDirPath)
{
    DIR* pDir = NULL;
    if (szDirPath == NULL)
        return false;

    pDir = opendir(szDirPath) ;
    if (pDir== NULL)
        return false;
    closedir(pDir);
     return true;
}



#if    ! KEYBOARD_INPUT_ENABLE

//磁盘文件 run.log 中存放着 LOG 信息，循环存放，所以最开始4个字节指明当前起始位置。
//从起始位置到文件末尾，再转回到开头，跳过前4个字节，到起始位置前一个字节结束。
//内存缓冲区中的内容跟磁盘文件中的内容一致，只是内存中前4个字节没有用到。

#define    LOG_FILE_LENGTH    (32*1024*1024)
static int cur_mem_pos  = sizeof(int);
static int cur_disk_pos = sizeof(int);
static char write_buffer[LOG_FILE_LENGTH];
static char log_file_name[64] = "";

void CUtility_SaveLogFromMemoryToDisk()
{
    if (cur_mem_pos == cur_disk_pos)
        return;

    FILE* fp;
    if (strlen(log_file_name) == 0)
    {
        strcpy(log_file_name, "run.log.");

        time_t now;
        time(&now);
        char str[16];
        sprintf(str, "%d", (int)now & 0xfff);
        strcat(log_file_name, str);

        fp = fopen(log_file_name, "wb");
    }
    else
        fp = fopen(log_file_name, "rb+");

    if (!fp)
        return;

    fseek(fp, cur_disk_pos, SEEK_SET);
    if (cur_mem_pos >= cur_disk_pos)
        fwrite(&(write_buffer[cur_disk_pos]), cur_mem_pos-cur_disk_pos, 1, fp);
    else
    {
        fwrite(&(write_buffer[cur_disk_pos]), LOG_FILE_LENGTH-cur_disk_pos, 1, fp);
        fseek(fp, sizeof(int), SEEK_SET);
        fwrite(&(write_buffer[sizeof(int)]), cur_mem_pos-sizeof(int), 1, fp);
    }

    cur_disk_pos = cur_mem_pos;

    fseek(fp, 0, SEEK_SET);
    fwrite(&cur_disk_pos, sizeof(int), 1, fp);
    fclose(fp);
}

void CUtility_Printf(const char* format, ...)
{
    char szBuffer[512];
    memset(szBuffer, 0x00, sizeof(szBuffer));

    time_t tBgnTime = time(0);
    struct tm* local;
    local = localtime(&tBgnTime);  //转换成当前时区的时间
    strftime(szBuffer, sizeof(szBuffer), "%Y-%m-%d %H:%M:%S  ", local);
    int len = strlen(szBuffer);

    va_list ap;
    va_start(ap, format);
    vsnprintf(szBuffer+len, sizeof(szBuffer)-len-1, format, ap); // -1 非常重要，避免 strlen 的长度超过 512 很多
    va_end(ap);

    len = strlen(szBuffer);
    if ( (cur_mem_pos + len) <= LOG_FILE_LENGTH )
    {
        memcpy(&(write_buffer[cur_mem_pos]), szBuffer, len);
        cur_mem_pos += len;
        if (cur_mem_pos == LOG_FILE_LENGTH)
            cur_mem_pos = sizeof(int);
    }
    else
    {
        int lenFirst = LOG_FILE_LENGTH-cur_mem_pos;
        memcpy(&(write_buffer[cur_mem_pos]), szBuffer, lenFirst);

        len -= lenFirst;
        memcpy(&(write_buffer[sizeof(int)]), szBuffer+lenFirst, len);
        cur_mem_pos = len + sizeof(int);
    }

    static int count = 0;
    if ( ((count++) & 15) == 0 ) //每n次往磁盘写一次
        CUtility_SaveLogFromMemoryToDisk();
}
#endif

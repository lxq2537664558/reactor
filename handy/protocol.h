#pragma once
#include <string>
#include <string.h>
#include <arpa/inet.h>
#define MSGHEAD_LEN 73
#define MSG_CHAT_MSG	0x0001
#define MSG_CHAT_MSG_RSP	0x0006

#define MSG_LOGIN_MSG	0x0002
#define MSG_LOGIN_MSG_RSP	0x0004

#define MSG_HEARBREAK_MSG	0x0003
#define MSG_HEARBREAK_MSG_RSP	0x0005
#define MSG_MASS_MSG	0x0007

#define RETOCDE_LOGIN_SUC "00"
#define RETOCDE_PWD_ERROR "01"		//密码错
#define RETOCDE_USER_ERROR "02"		//用户账号错

#pragma pack(1)
struct MsgHeader				//心跳直接发送BaseHeader
{
	unsigned char start;		//起始位
	unsigned short version;		//版本号
	unsigned short cmd;			//命令
	unsigned char sessionID[8];			//消息的标识。
	unsigned char sender[32];	//发送者账号
	unsigned char receiver[32];	//接收者账号
	int contxLen;				//消息上下文长度	
	MsgHeader()
	{
		start = 0x02;
		version = 0x0001;
		memset(sessionID,0,8);
		memset(sender, 0, 32);
		memset(receiver, 0, 32);
		contxLen = 0;
		cmd = 0;
	}
	void Hton(){
		version = htons(version);
		cmd = htons(cmd);
		contxLen = htonl(contxLen);
	}

	void Ntoh(){
		version = ntohs(version);
		cmd = ntohs(cmd);
		contxLen = ntohl(contxLen);
	}
};
struct SMsg
{
	MsgHeader baseHeader;
	std::string msgContx;			//消息上下文
	SMsg(){ msgContx.clear(); }
};


class Protocol
{
public:
	explicit Protocol();
	~Protocol();
	std::string packageLoginMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8],unsigned char pwd[32]);
	std::string packageLoginMsgRsp(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8], unsigned char returnCode[2]);
	//聊天消息
	std::string packageChatMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8],std::string& strchat);	
	//群消息
	std::string packageMassMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8], std::string& strchat);
	std::string packageChatMsgRsp(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8]);
	std::string packageHeatbreakMsg(unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8]);

	//模拟BMP服务器消息返回
	std::string packageTradeMsgRsp();
private:
	MsgHeader _packageHeader(unsigned short cmd, unsigned char sender[32], unsigned char rever[32], unsigned char sessionID[8], int contxLen);
};
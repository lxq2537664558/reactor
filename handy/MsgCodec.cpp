#include "MsgCodec.h"
using namespace handy;

int MsgCodec::tryDecode(Slice data, Slice& msg) {
	if (data.size() < 70)
	{
		//printf("data len < msg header len,then decode msg failed\n");
		return 0;
	}

	msg = data;
	return msg.size();
}
void MsgCodec::encode(Slice msg, Buffer& buf) {
	buf.append(msg);
}



#include <string.h>
#include "MysqlOperator.h"
#include "MsgHandler.h"
#include "protocol.h"
#include "Utility.h"
#include "UserManager.h"
#include "serveripconfig.h"
using namespace std;
using namespace handy;

MsgHandler::MsgHandler()
{
}


MsgHandler::~MsgHandler()
{
}
string MsgHandler::_HandleLoginMsg(const char* szData, int len, bool& bLogin)
{
	MsgHeader smsg;	
	memcpy((void*)&smsg, (void*)szData, sizeof(MsgHeader));
	smsg.Ntoh();
	string conxt;
	conxt.append((char*)szData + sizeof(smsg), smsg.contxLen);
	info("rev login msg \nmsg id [0x%0x] msg ver [0x%0x] msg cmd [0x%0x]  msg sender [%s] ---> msg receiver [%s] msg contx len [%u] msg contx  [%s]\n",
		smsg.start, smsg.version, smsg.cmd, smsg.sender, smsg.receiver, smsg.contxLen, conxt.c_str());		
	string amount;
	amount.append((char*)(smsg.sender), 32);
	CMysqlOperator mysqlOpt;
	SMapRows mapRow;
	string sql = CUtility::Format("SELECT * FROM im_user WHERE username=\'%s\'", (char*)smsg.sender);	
	mysqlOpt.Select(db_svrip.c_str(), "3319", "root", "admin", "im_db", sql.c_str(), mapRow);
	bLogin = false;
	unsigned char code[2];
	if (!mapRow.IsEmpty())
	{
		if (mapRow.GetRow(0)->mapRow["password"].pStrData->compare(conxt.c_str()) == 0)
		{						
			bLogin = true;
			memcpy(code, RETOCDE_LOGIN_SUC, 2);			
		}
		else
		{
			memcpy(code, RETOCDE_PWD_ERROR, 2);
		}
	}
	else
		memcpy(code, RETOCDE_USER_ERROR, 2);
	Protocol protocol;
	string sender = (char*)smsg.receiver;
	string revder = (char*)smsg.sender;
	string session = (char*)smsg.sessionID;
	string msg = protocol.packageLoginMsgRsp((unsigned char*)sender.c_str(), (unsigned char*)revder.c_str(), (unsigned char*)session.c_str(), code);
	return msg;
}
void MsgHandler::_HandleLoginMsgRsp(const char* szData, int len)
{
	MsgHeader smsg;
	memcpy((void*)&smsg, (void*)szData, sizeof(MsgHeader));
	smsg.Ntoh();
	string conxt;
	conxt.append((char*)szData + sizeof(smsg), smsg.contxLen);
	if (conxt.compare(RETOCDE_LOGIN_SUC)==0)
		info("%s login suc\n", (char*)smsg.receiver);
	else
		info("%s login false %s\n", (char*)smsg.receiver, conxt.c_str());
}

void MsgHandler::_HandleChatMsg(const handy::TcpConnPtr& con,const char* szData, int len)
{	
	string conxt;
	MsgHeader smsg;
	memcpy((void*)&smsg, (void*)szData, sizeof(MsgHeader));
	smsg.Ntoh();
	conxt.append((char*)((char*)(szData) + sizeof(smsg)), smsg.contxLen);
	info("msg id [0x%0x] msg ver [0x%0x] msg cmd [0x%0x]  msg sender [%s] msg receiver [%s] msg contx len [%u] msg contx  [%s]\n",
		smsg.start, smsg.version, smsg.cmd, smsg.sender, smsg.receiver, smsg.contxLen, conxt.c_str());
#ifndef CHAT_CLIENT
	string receiver,sender;
	receiver=(char*)smsg.receiver;//receiver是32字节，只取带字符部分	
	TcpConnPtr revPtr;		

	if (gUserManager->FindUser(receiver, revPtr))
	{
		Protocol protocol;
		string chatMsg;
		smsg.Hton();
		chatMsg.append((char*)&smsg, sizeof(smsg));
		chatMsg.append(conxt.c_str(), conxt.length());
		revPtr->send(chatMsg);
		info("Transfer chat msg [%s] ---> [%s]\n", conxt.c_str(), (char*)smsg.receiver);
	}
	else
	{		
		info("recoder offline msg\n");
		INT64 tick = CUtility::GetTickTime();
		string strtick = CUtility::Format("%lld", tick);
		strtick = strtick.substr(0, 10);
		CMysqlOperator mysqlOpt;		
		string sql = CUtility::Format("INSERT INTO im_message VALUE(\'\',\'%s\',\'%s\',\'%s\',\'%s\')", (char*)smsg.sender, (char*)smsg.receiver, conxt.c_str(), strtick.c_str());
		if (mysqlOpt.Connect(phpip.c_str(), 3306, "root", "quyongjiu1990", "im_db"))
			mysqlOpt.Execute(sql.c_str());		
	}	
#endif
}

void MsgHandler::_HandleMassMsg(const char* szData, int len)
{
	string conxt;
	MsgHeader smsg;
	memcpy((void*)&smsg, (void*)szData, sizeof(MsgHeader));
	smsg.Ntoh();
	conxt.append((char*)((char*)(szData)+sizeof(smsg)), smsg.contxLen);
	info("msg id [0x%0x] msg ver [0x%0x] msg cmd [0x%0x ] msg sender [%s]---> msg receiver [%s] msg contx len [%u] msg contx [%s]\n",
		smsg.start, smsg.version, smsg.cmd, smsg.sender, smsg.receiver, smsg.contxLen, conxt.c_str());
#ifndef CHAT_CLIENT
	string sender = (char*)smsg.sender;
	string group = (char*)smsg.receiver;
	
	CMysqlOperator mysqlOpt;
	SMapRows mapRow;
	string sql = CUtility::Format("SELECT * FROM im_member WHERE \`group_name\`=\'%s\'", (char*)smsg.receiver);
	mysqlOpt.Select(db_svrip.c_str(), "3319", "root", "admin", "im_db", sql.c_str(), mapRow);
	if (!mapRow.IsEmpty())
	{
		int i = 0;
		smsg.Hton();
		while (mapRow.GetRow(i))
		{
			string receiver = mapRow.GetRow(i)->mapRow["username"].pStrData->c_str();
			TcpConnPtr revPtr;
			if (gUserManager->FindUser(receiver, revPtr))
			{
				string chatMsg;				
				chatMsg.append((char*)&smsg, sizeof(smsg));
				chatMsg.append(conxt.c_str(), conxt.length());
				revPtr->send(chatMsg);				
			}
			else
			{
				info("storage mass msg msg\n");
				INT64 tick = CUtility::GetTickTime();
				string strtick = CUtility::Format("%lld", tick);
				strtick = strtick.substr(0, 10);
				CMysqlOperator mysqlOpt;
				string sql = CUtility::Format("INSERT INTO im_group_msg VALUE(\'\',\'%s\',\'%s\',\'%s\',\'%s\')", group.c_str(),sender.c_str(),conxt.c_str(), strtick.c_str());
				if (mysqlOpt.Connect(phpip.c_str(), 3306, "root", "quyongjiu1990", "im_db"))
					mysqlOpt.Execute(sql.c_str());
			}
			i++;
		}		
	}
#endif
}
string Asc2Bcd(const char* strAsc, int len);
std::string Asc2Bcd(string & strAsc);
string MsgHandler::HandleMsg(const TcpConnPtr& con,const char* szData, int len)
{
	string data;
	data.append(szData, len);		
	int pb = 0;
	string output;
	do
	{
		MsgHeader smsg;
		char*  pdata = (char*)data.c_str();
		memcpy((void*)&smsg, (void*)pdata, sizeof(MsgHeader));
		smsg.Ntoh();
		if (smsg.start != 0x02)
		{
			info("package is wrong\n");
			break;
		}

		if (smsg.cmd != MSG_HEARBREAK_MSG)
			pb = sizeof(smsg) + smsg.contxLen;
		else
			pb = sizeof(smsg);
		
		bool bMsg = true;
		switch (smsg.cmd)
		{
		case MSG_CHAT_MSG:
			info("%s rev chat msg\n", (char*)smsg.receiver);
			_HandleChatMsg(con, pdata, pb);			
			break;
		case MSG_MASS_MSG:
			info("%s rev mass msg\n", (char*)smsg.receiver);
			_HandleMassMsg(pdata, pb);
			break;
		case MSG_LOGIN_MSG:
		{			
			bool bLogin;
			string msg = _HandleLoginMsg(pdata, pb, bLogin);
			output.append(msg.c_str(), msg.length());
			if (bLogin)
			{
				info("Add user %s to manager %p\n", (char*)smsg.sender, con);
				gUserManager->AddUser((char*)smsg.sender, con);				
			}
			else{
				info("%s  login false \n", (char*)smsg.sender);
			}
		}

		break;
		case MSG_HEARBREAK_MSG:
			info("%s rev heart break msg\n", (char*)smsg.receiver);
			smsg.Hton();
			output.append((char*)&smsg, sizeof(MsgHeader));
			break;
		case MSG_HEARBREAK_MSG_RSP:
			info("rev heartbreak msg rsp\n");
			break;
		case MSG_LOGIN_MSG_RSP:
			info("rev login rsp msg");
			_HandleLoginMsgRsp(pdata, pb);
			break;
		default:
			bMsg = false;
			info("unknown msg cmd\n");			
			break;
		}
		if (!bMsg)
		{
			break;
		}

		if (data.length() > sizeof(MsgHeader))
		{
			data = data.substr(pb, data.length() - pb);
			if (data.size() < sizeof(MsgHeader))
				break;
		}
		else
		{
			break;
		}
	} while (true);
	Protocol protocol;
	string msg;
	string msgcontx=protocol.packageTradeMsgRsp();
	string  strlen = std::to_string(msgcontx.length());
	if (strlen.length()<4)
	{
		int n = 4 - strlen.length();
		string s;
		while (n--)
		{		
			s += "0";
		}
		s += strlen;
		strlen = s;
	}
	string tpdu = "6000000060";
	msg += Asc2Bcd(tpdu);
	msg += strlen;	
	msg += "01";
	msg+="01";		
	msg += msgcontx;

	string t = msg;
	msg.clear();
	unsigned short len = t.length();
	len = len << 8 | len >> 8;
	msg.append((char*)&len, 2);
	msg += t;
	output = msg;
	return output;
}
typedef unsigned char byte;
std::string Asc2Bcd(string & strAsc)
{
	return Asc2Bcd(strAsc.c_str(), strAsc.length());
}
string Asc2Bcd(const char* strAsc, int len)
{
	string strBCD;
	int	i;
	byte	str[2];
	memset(str, 0, sizeof(str));
	for (i = 0; i < len; i += 2){
		if ((strAsc[i] >= 'a') && (strAsc[i] <= 'f'))
			str[0] = strAsc[i] - 'a' + 0x0A;
		else if ((strAsc[i] >= 'A') && (strAsc[i] <= 'F'))
			str[0] = strAsc[i] - 'A' + 0x0A;
		else if (strAsc[i] >= '0')
			str[0] = strAsc[i] - '0';
		else
			str[0] = 0;

		if ((strAsc[i + 1] >= 'a') && (strAsc[i + 1] <= 'f'))
			str[1] = strAsc[i + 1] - 'a' + 0x0A;
		else if ((strAsc[i + 1] >= 'A') && (strAsc[i + 1] <= 'F'))
			str[1] = strAsc[i + 1] - 'A' + 0x0A;
		else if (strAsc[1] >= '0')
			str[1] = strAsc[i + 1] - '0';
		else
			str[1] = 0;

		byte b = (str[0] << 4) | (str[1] & 0x0F);
		byte* p = &b;
		char c;
		memcpy(&c, &b, 1);
		strBCD.append(1, c);
	}
	return strBCD;
}